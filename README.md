# EXPEDITUS #

## Overview ##
Expeditus is a small 2D adventure videogame, with a bird’s-eye view style like Pokémon. 
The title is latin for quick, armed, ready.

Fundamental is the trading-card system: the player will be able to collect and use cards in order to perform every action.

## Cards ##
There are four types of cards:
Object cards: they represent an object that can be used by the player, like a potion or a dress.
Utility cards: they represent utilites and tools that can be used by the player, like the ability to quickly travel to any point of the map.
Battle cards: they represents attacks or defense that can be performed by the player during battles.
Special cards: they are rare and powerful cards, that give a great advantage to the player.

Further details on the game will be shared as soon as possible.
C++ is used, with Lua for scripting.

### Compilation ###
If you want to download and compile the code by yourself, remember to put the final executable file into 'Debug' directory.