/*
 * Tool per il progetto Expeditus
 * Converte un file di testo in formato binario
 */

#include <iostream>
#include <fstream>
#include <sstream>
#include <string>
#include <cctype>
#include "..\Expeditus\Consts.h"

bool checkeof(std::ifstream *stream, int pos, bool quit=true){
	if (stream->eof()){
		if (quit){
			std::cerr << "Reached end-of-file: syntax error in position " << pos << ". Exit" << std::endl;
			exit(1);
		}
		return false;
	}
	return true;
}

void lowerstr(std::string &str){
	for (int i = 0; i < str.length(); i++)
		str[i] = tolower(str[i]);
}

void cards(){
	std::ifstream input;
	std::ofstream output;
	/* Use .txt file */
	std::string input_path, output_path;
	std::cout << "Input filepath: ";
	std::cin >> input_path;
	std::cout << "Output filepath: ";
	std::cin >> output_path;
	input.open(input_path);
	output.open(output_path, std::ios::out | std::ios::binary | std::ios::trunc);
	if (!input.is_open()){
		std::cerr << "Cannot open input file. Exit." << std::endl;
		exit(1);
	}
	if (!output.is_open()){
		std::cerr << "Cannot open output file. Exit." << std::endl;
		exit(1);
	}

	/* Do things */
	int uid, limit;
	std::string name, type, desc;
	std::string temp;
	std::stringstream s_temp;

	output.seekp(0, std::ios::beg);

	while (!input.eof()){
		/* Read the text file */
		// UID
		do{
			getline(input, temp);
		} while (temp.empty());
		if (temp.length() <= 5){
			std::cerr << "Syntax error in 'uid' in position " << (uid + 1) << std::endl;
			exit(1);
		}
		s_temp.clear();
		s_temp.str(temp.substr(5));
		s_temp >> uid;
		// Name
		checkeof(&input, uid);
		temp.clear();
		do{
			getline(input, temp);
		} while (temp.empty());
		if (temp.length() <= 6){
			std::cerr << "UID " << uid << " Syntax error for 'name'" << std::endl;
			exit(1);
		}
		name = temp.substr(6);
		if (name.length() > EXPEDITUS_CARD_NAME_SIZE){
			std::cerr << "UID " << uid << " Name longer than maximum size (" << EXPEDITUS_CARD_NAME_SIZE << ")" << std::endl;
			exit(1);
		}
		// Type
		checkeof(&input, uid);
		temp.clear();
		do{
			getline(input, temp);
		} while (temp.empty());
		if (temp.length() <= 6){
			std::cerr << "UID " << uid << " Syntax error for 'type'" << std::endl;
			exit(1);
		}
		type = temp.substr(6);
		if (type.length() > EXPEDITUS_CARD_TYPE_SIZE){
			std::cerr << "UID " << uid << "Type longer than maximum size (" << EXPEDITUS_CARD_TYPE_SIZE << ")" << std::endl;
			exit(1);
		}
		lowerstr(type);
		if (type != "battle" && type != "object" && type != "utility" && type != "special"){
			std::cerr << "UID " << uid << " Syntax error for 'type'" << std::endl;
			exit(1);
		}
		// Limit
		checkeof(&input, uid);
		temp.clear();
		do{
			getline(input, temp);
		} while (temp.empty());
		if (temp.length() <= 7){
			std::cerr << "UID " << uid << " Syntax error for 'limit'" << std::endl;
			exit(1);
		}
		s_temp.clear();
		s_temp.str(temp.substr(7));
		s_temp >> limit;
		// Description
		checkeof(&input, uid);
		temp.clear();
		do{
			getline(input, temp);
		} while (temp.empty());
		if (temp.length() <= 6){
			std::cerr << "UID " << uid << " Syntax error in 'desc'" << std::endl;
			exit(1);
		}
		desc = temp.substr(6);
		if (desc.length() > EXPEDITUS_CARD_DESC_SIZE){
			std::cerr << "UID " << uid << " 'desc' longer than maximum size (" << EXPEDITUS_CARD_DESC_SIZE << ")" << std::endl;
			exit(1);
		}

		/* Write the binary file */
		output.write((char*)&uid, sizeof(int));
		output.write(name.c_str(), EXPEDITUS_CARD_NAME_SIZE);
		output.write(type.c_str(), EXPEDITUS_CARD_TYPE_SIZE);
		output.write((char*)&limit, sizeof(int));
		output.write(desc.c_str(), EXPEDITUS_CARD_DESC_SIZE);
	}

	input.close();
	output.close();

	/********* TEST *********/
	/*std::cout << "**** TEST ****" << std::endl;
	input.open(EXPEDITUS_FILE_CARDLIST, std::ios::in | std::ios::binary | std::ios::ate);
	if (!input.is_open()){
		std::cerr << "Cannot open binary file. Exit." << std::endl;
		exit(1);
	}
	std::streampos end = input.tellg();
	input.seekg(0, std::ios::beg);
	int auid, alimit;
	char aname[EXPEDITUS_CARD_NAME_SIZE], atype[EXPEDITUS_CARD_TYPE_SIZE], adesc[EXPEDITUS_CARD_DESC_SIZE];
	while (input.tellg() != end){
		aname[0] = atype[0] = adesc[0] = '\0';
		std::cout << "new cycle" << std::endl;
		input.read((char*)&auid, sizeof(int));
		std::cout << "uid = " << auid << std::endl;
		input.read(aname, EXPEDITUS_CARD_NAME_SIZE);
		std::cout << "name = " << aname << std::endl;
		input.read(atype, EXPEDITUS_CARD_TYPE_SIZE);
		std::cout << "type = " << atype << std::endl;
		input.read((char*)&alimit, sizeof(int));
		std::cout << "limit = " << alimit << std::endl;
		input.read(adesc, EXPEDITUS_CARD_DESC_SIZE);
		std::cout << "desc = " << adesc << std::endl;
	}*/
}

void dialogues(){
	std::ifstream input;
	std::ofstream output;

	std::string input_path, output_path;
	std::cout << "Input filepath: ";
	std::cin >> input_path;
	std::cout << "Output filepath: ";
	std::cin >> output_path;
	input.open(input_path);
	output.open(output_path, std::ios::out | std::ios::binary | std::ios::trunc);
	if (!input.is_open()){
		std::cerr << "Cannot open input file. Exit." << std::endl;
		exit(1);
	}
	if (!output.is_open()){
		std::cerr << "Cannot open output file. Exit." << std::endl;
		exit(1);
	}

	/* Do things */
	int uid, next, pos;
	std::string name, text;
	bool answer, script, complete;
	std::string temp;
	std::stringstream s_temp;

	output.seekp(0, std::ios::beg);

	while (!input.eof()){
		next = 0;
		answer = script = complete = false;
		/* Read the text file */
		// UID
		do{
			getline(input, temp);
		} while (temp.empty());
		if (temp.length() <= 5){
			std::cerr << "Syntax error in 'uid' in position " << (uid + 1) << std::endl;
			exit(1);
		}
		s_temp.clear();
		s_temp.str(temp.substr(5));
		s_temp >> uid;
		// Name
		checkeof(&input, uid);
		temp.clear();
		do{
			getline(input, temp);
		} while (temp.empty());
		if (temp.length() <= 6)
			name = "";
		else
			name = temp.substr(6);
		if (name.length() > EXPEDITUS_CHARACTER_NAME_SIZE){
			std::cerr << "UID " << uid << " Character name longer than maximum size (" << EXPEDITUS_CHARACTER_NAME_SIZE << ")" << std::endl;
			exit(1);
		}
		// Text
		checkeof(&input, uid);
		temp.clear();
		do{
			getline(input, temp);
		} while (temp.empty());
		if (temp.length() <= 6){
			std::cerr << "UID " << uid << " Syntax error for 'text'" << std::endl;
			exit(1);
		}
		text = temp.substr(6);
		temp.clear();
		while (temp != "TEXTEND"){
			if (!temp.empty())
				text += '\n' + temp;
			checkeof(&input, uid);
			getline(input, temp);
		}
		if (text.length() > EXPEDITUS_DIALOGUE_TEXT_MAX_SIZE){
			std::cerr << "UID " << uid << " 'text' longer than maximum size (" << EXPEDITUS_DIALOGUE_TEXT_MAX_SIZE << ")" << std::endl;
			exit(1);
		}
		int textLength = text.length();
		// Next UID
		if (checkeof(&input, uid, false)){
			temp.clear();
			pos = input.tellg();
			getline(input, temp);
			if (temp.empty()) complete = true;
			else{
				if (temp.length() <= 6){
					std::cerr << "Syntax error in position " << uid << std::endl;
					exit(1);
				}
				if (temp.substr(0, 5) == "NEXT:"){
					s_temp.clear();
					s_temp.str(temp.substr(6));
					s_temp >> next;
				}
				else{
					input.seekg(pos);
				}
			}
		}
		// Answer
		if (!complete && checkeof(&input, uid, false)){
			temp.clear();
			pos = input.tellg();
			getline(input, temp);
			if (temp.empty()) complete = true;
			else{
				if (temp.length() <= 8){
					std::cerr << "Syntax error in position " << uid << std::endl;
					exit(1);
				}
				int length = temp.length();
				if (temp.substr(0, 7) == "ANSWER:"){
					temp = temp.substr(8);
					lowerstr(temp);
					if (temp == "yes")
						answer = true;
					else if (temp == "no")
						answer = false;
					else{
						std::cerr << "Syntax error in 'answer' in position " << uid << std::endl;
						exit(1);
					}
				}
				else{
					input.seekg(pos);
				}
			}
		}
		// Script
		if (!complete && checkeof(&input, uid, false)){
			temp.clear();
			getline(input, temp);
			if (!temp.empty()){
				if (temp.length() <= 5){
					std::cerr << "Syntax error in position " << uid << std::endl;
					exit(1);
				}
				if (temp.length() <= 7 && temp.substr(0, 7) == "SCRIPT:"){
					temp = temp.substr(8);
					lowerstr(temp);
					if (temp == "yes")
						script = true;
					else if (temp == "no")
						script = false;
					else{
						std::cerr << "Syntax error in 'script' in position " << uid << std::endl;
						exit(1);
					}
				}
			}
		}

		/* Write the binary file */
		output.write((char*)&uid, EXPEDITUS_UID_SIZE);
		output.write(name.c_str(), EXPEDITUS_CARD_NAME_SIZE);
		output.write((char*)&textLength, sizeof(int));
		output.write(text.c_str(), textLength);
		output.write((char*)&next, EXPEDITUS_UID_SIZE);
		output.write((char*)&answer, sizeof(bool));
		output.write((char*)&script, sizeof(bool));
	}

	input.close();
	output.close();

	/********* TEST *********/
	/*std::cout << "**** TEST ****" << std::endl;
	input.open(output_path, std::ios::in | std::ios::binary | std::ios::ate);
	if (!input.is_open()){
		std::cerr << "Cannot open binary file. Exit." << std::endl;
		exit(1);
	}
	std::streampos end = input.tellg();
	input.seekg(0, std::ios::beg);
	int auid, anext, asize;
	char aname[EXPEDITUS_CHARACTER_NAME_SIZE], atext[EXPEDITUS_DIALOGUE_TEXT_MAX_SIZE];
	bool aanswer, ascript;
	while (input.tellg() != end){
		aname[0] = atext[0] = '\0';
		std::cout << "new cycle" << std::endl;
		input.read((char*)&auid, EXPEDITUS_UID_SIZE);
		std::cout << "uid = " << auid << std::endl;
		input.read(aname, EXPEDITUS_CHARACTER_NAME_SIZE);
		std::cout << "name = " << aname << std::endl;
		input.read((char*)&asize, sizeof(int));
		std::cout << "size = " << asize << std::endl;
		input.read(atext, asize);
		atext[asize] = '\0';
		std::cout << "text = " << atext << std::endl;
		input.read((char*)&anext, EXPEDITUS_UID_SIZE);
		std::cout << "next = " << anext << std::endl;
		input.read((char*)&aanswer, sizeof(bool));
		std::cout << "answer = " << aanswer << std::endl;
		input.read((char*)&ascript, sizeof(bool));
		std::cout << "script = " << ascript << std::endl;
	}*/
}

void map(){
	std::string input_path, output_path;
	std::cout << "Input filepath: ";
	std::cin >> input_path;
	std::cout << "Output filepath: ";
	std::cin >> output_path;

	std::ifstream input;
	std::ofstream output;
	/* Use .txt file */
	input.open(input_path);
	output.open(output_path, std::ios::out | std::ios::binary | std::ios::trunc);
	if (!input.is_open()){
		std::cerr << "Cannot open input file. Exit." << std::endl;
		exit(1);
	}
	if (!output.is_open()){
		std::cerr << "Cannot open output file. Exit." << std::endl;
		exit(1);
	}

	/* Do things */
	int count = 1;
	int width, height;
	int posX, posY;
	bool movement, event=false; ////// WIP

	output.seekp(0, std::ios::beg);

	/* Map width */
	checkeof(&input, 0);
	input >> width;
	if (width > EXPEDITUS_MAP_MAX_WIDTH){
		std::cerr << "Map width greater than maximum value " << EXPEDITUS_MAP_MAX_WIDTH << std::endl;
		exit(1);
	}
	output.write((char*)&width, sizeof(int));

	/* Map height */
	checkeof(&input, 0);
	input >> height;
	if (height > EXPEDITUS_MAP_MAX_HEIGHT){
		std::cerr << "Map height greater than maximum value " << EXPEDITUS_MAP_MAX_HEIGHT << std::endl;
		exit(1);
	}
	output.write((char*)&height, sizeof(int));

	/* Player position */
	checkeof(&input, 0);
	input >> posX;
	if (posX < 0 || posX > width - 1){
		std::cerr << "Invalid player x position" << std::endl;
		exit(1);
	}
	output.write((char*)&posX, sizeof(int));
	checkeof(&input, 0);
	input >> posY;
	if (posY < 0 || posY > height - 1){
		std::cerr << "Invalid player x position" << std::endl;
		exit(1);
	}
	output.write((char*)&posY, sizeof(int));

	/* Map tiles */
	while (!input.eof()){
		/* Read the text file */
		// Movement permission
		checkeof(&input, count);
		input >> movement;
		// Event
		//checkeof(&input, count);
		//input >> event;

		/* Write the binary file */
		output.write((char*)&movement, sizeof(bool));
		output.write((char*)&event, sizeof(bool));

		count++;
	}

	input.close();
	output.close();

	/********* TEST *********/
	/*std::cout << "**** TEST ****" << std::endl;
	input.open(output_path, std::ios::in | std::ios::binary | std::ios::ate);
	if (!input.is_open()){
		std::cerr << "Cannot open binary file. Exit." << std::endl;
		exit(1);
	}
	std::streampos end = input.tellg();
	input.seekg(0, std::ios::beg);
	int awidth, aheight;
	input.read((char*)&awidth, sizeof(int));
	input.read((char*)&aheight, sizeof(int));
	std::cout << "Map width = " << awidth << "; height = " << aheight << std::endl;
	int aposx, aposy;
	input.read((char*)&aposx, sizeof(int));
	input.read((char*)&aposy, sizeof(int));
	std::cout << "Pos x = " << aposx << "; y = " << aposy << std::endl;
	bool amovement, aevent;
	while (input.tellg() != end){
		amovement = aevent = false;
		std::cout << "new cycle" << std::endl;
		input.read((char*)&amovement, sizeof(bool));
		std::cout << "movement = " << amovement << std::endl;
		input.read((char*)&aevent, sizeof(bool));
		std::cout << "event = " << aevent << std::endl;
	}*/
}

int main(){
	/* Choose file */
	std::cout << "1) Card list" << std::endl;
	std::cout << "2) Dialogues" << std::endl;
	std::cout << "3) Map" << std::endl;
	std::cout << "Which file do you want to transform in binary? ";
	int chose;
	do{
		std::cin >> chose;
	} while (chose < 0 || chose > 3);
	switch (chose){
	case 1:
		cards();
		break;
	case 2:
		dialogues();
		break;
	case 3:
		map();
		break;
	}
	std::cout << "Done. :)" << std::endl;
	return 0;
}