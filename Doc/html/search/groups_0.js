var searchData=
[
  ['constants_20about_20card_20and_20deck_20size_2e',['Constants about card and deck size.',['../group___c_a_r_d_s_i_z_e.html',1,'']]],
  ['constants_20about_20dialogues_27s_20size_2c_20position_20and_20delay_2e',['Constants about dialogues&apos;s size, position and delay.',['../group___d_i_a_l_o_g_u_e_s.html',1,'']]],
  ['constants_20about_20file_20paths_2e',['Constants about file paths.',['../group___f_i_l_e_p_a_t_h_s.html',1,'']]],
  ['constants_20about_20fps_20value_2e',['Constants about fps value.',['../group___f_p_s.html',1,'']]],
  ['constants_20about_20map_27s_20size_2e',['Constants about map&apos;s size.',['../group___m_a_p_s.html',1,'']]]
];
