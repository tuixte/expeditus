var class_player =
[
    [ "Player", "class_player.html#ab382e40eba2c2175cff8063fea3cb234", null ],
    [ "addCard", "class_player.html#a024c5ca4dae955a4b7e8c2a3abbf6f19", null ],
    [ "getAlbum", "class_player.html#aa6a0876381aa9856a956505d05c1f519", null ],
    [ "getName", "class_player.html#ad4c6a95f1cf69c44d0c585465b101c70", null ],
    [ "owned", "class_player.html#a22ebb9d77bd085c797c6ab7ed8bfb577", null ],
    [ "removeCard", "class_player.html#a2506e2646d0efc53fb411fb458e97ae7", null ],
    [ "setName", "class_player.html#a95d2b46eee230ad3e31612ff2bc681da", null ],
    [ "SavingInterface", "class_player.html#a64289eae282d40626db7d4818c090ed8", null ],
    [ "name", "class_player.html#af9c920fabaafdeb7961a645315b521ff", null ]
];