var class_log =
[
    [ "flags", "class_log.html#a658b18b3f472034fdd3b8381df95dc99", [
      [ "text", "class_log.html#a658b18b3f472034fdd3b8381df95dc99a560a8c21d984fd6d8a223591f933cb80", null ],
      [ "alert", "class_log.html#a658b18b3f472034fdd3b8381df95dc99a240163c5c15d7aa5cbbd89b0f5224152", null ],
      [ "error", "class_log.html#a658b18b3f472034fdd3b8381df95dc99a778a63c1cfdf58e003bc7be631eb459a", null ],
      [ "critic", "class_log.html#a658b18b3f472034fdd3b8381df95dc99acf60e670c11a505450d58db259ba30e1", null ]
    ] ],
    [ "flag", "class_log.html#af1d37f512df4fcbb3c76a87743491dde", null ],
    [ "flag", "class_log.html#ab8a85f7112aabd19615171f91f337cd8", null ],
    [ "log", "class_log.html#a66eef7fd7bba3c7f1effe77d23392158", null ],
    [ "log", "class_log.html#aa6ec94788fad570d6e09765aa698bc17", null ],
    [ "log", "class_log.html#a4e8febd94f5e5ba350323d5f5e9f7c20", null ],
    [ "newline", "class_log.html#a442a54b027ef9e3ae7e778b82d48c3c3", null ],
    [ "operator<<", "class_log.html#a7cf24eccb62418537958e20f0209413d", null ],
    [ "operator<<", "class_log.html#afb9293e53f70008c615b4e6f6650f592", null ],
    [ "operator<<", "class_log.html#a7ab0e08af58fa72fd6e45c8ef3a15e10", null ],
    [ "printTime", "class_log.html#a97a8661ebe447273618e59314b8ff89d", null ]
];