var class_character =
[
    [ "Character", "class_character.html#a87173058c0e06fb228f0051eb329b15d", null ],
    [ "collides", "class_character.html#a57749450e5ee54debb50f337ce02a2c9", null ],
    [ "draw", "class_character.html#aa1740ec19181a906fe39ab347d9a972b", null ],
    [ "getBounds", "class_character.html#a64ef01239352391777106c9b7b2412e7", null ],
    [ "getFromSheet", "class_character.html#a59dfd2822d4d7714219ac9c174aeb710", null ],
    [ "getPixelVelocity", "class_character.html#add137c93577fca617a70b82e9496f94d", null ],
    [ "getPosition", "class_character.html#ad58780c3579e6fef162635f4c49940ba", null ],
    [ "getSize", "class_character.html#ad98eea4eba7b5b815f099345e100488b", null ],
    [ "getTileVelocity", "class_character.html#a793302ffa97f351bc2a906b5f7d093f4", null ],
    [ "isNear", "class_character.html#af9370f570b0ca262fd838a42b2c74300", null ],
    [ "load", "class_character.html#a77fbeafd095ba254cfa4bd1aa6a4fd92", null ],
    [ "move", "class_character.html#a70a0bd0aaa516b8b14bc80843c6b247f", null ],
    [ "moveTiles", "class_character.html#a61113ab5268b6455a831c07a4bddf731", null ],
    [ "setPixelVelocity", "class_character.html#aeea3855965e3124aa7e17ba58e081237", null ],
    [ "setPosition", "class_character.html#ab53ba8869f52048633d35553cf6c995f", null ],
    [ "setPosition", "class_character.html#a44aaac88a2e46e125358a55fca8566fa", null ],
    [ "setTileVelocity", "class_character.html#abc6e33458ccd01f71a0ff0642aa9afe6", null ],
    [ "update", "class_character.html#af8bcd2b43032e97dfd12d1c6fbd83be8", null ],
    [ "TiledMap", "class_character.html#acec4b5e650013e7b00a34bf8445fcef7", null ],
    [ "map", "class_character.html#a02dad038c360501e524ad363c699956e", null ],
    [ "uid", "class_character.html#ae0048bc2ccfde6e99dd81f8033cf7219", null ]
];