var class_deck =
[
    [ "Deck", "class_deck.html#a57ae1cb4ac6fd61c249cefb2db85eb99", null ],
    [ "addCard", "class_deck.html#aac6cb67ce4b441c21e4b396aab35cf69", null ],
    [ "count", "class_deck.html#ad8264ff9bdaaed8bdc2b983ee42ffbf2", null ],
    [ "delCardByPos", "class_deck.html#a5a9d7b6c5431badd8d469bfb0282b04f", null ],
    [ "delCardByUID", "class_deck.html#aaf23436537946beed53f854f80ce1b40", null ],
    [ "getCardByPos", "class_deck.html#a7f2f5490cb01cbf41e7c55c7ebf214a2", null ],
    [ "getCardByUID", "class_deck.html#a4097a627eb57f6dd760e3d5174eda4f8", null ]
];