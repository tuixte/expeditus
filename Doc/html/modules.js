var modules =
[
    [ "Constants about file paths.", "group___f_i_l_e_p_a_t_h_s.html", "group___f_i_l_e_p_a_t_h_s" ],
    [ "Constants about card and deck size.", "group___c_a_r_d_s_i_z_e.html", "group___c_a_r_d_s_i_z_e" ],
    [ "Constants about dialogues's size, position and delay.", "group___d_i_a_l_o_g_u_e_s.html", "group___d_i_a_l_o_g_u_e_s" ],
    [ "Constants about map's size.", "group___m_a_p_s.html", "group___m_a_p_s" ],
    [ "Constants about fps value.", "group___f_p_s.html", "group___f_p_s" ]
];