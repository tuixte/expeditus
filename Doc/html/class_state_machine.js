var class_state_machine =
[
    [ "states", "class_state_machine.html#a51712600fee6bf7be2b7839f1661b831", [
      [ "previous", "class_state_machine.html#a51712600fee6bf7be2b7839f1661b831ada1a96e8220c0f16e0b6af6456caa6cd", null ],
      [ "introduction", "class_state_machine.html#a51712600fee6bf7be2b7839f1661b831ae934454cd1997b073ebf1e44ab257f38", null ],
      [ "game", "class_state_machine.html#a51712600fee6bf7be2b7839f1661b831a2647a09d593497eec97f18c828437190", null ]
    ] ],
    [ "StateMachine", "class_state_machine.html#aa7488f99ae23f6ad675fb5cec9d2a730", null ],
    [ "~StateMachine", "class_state_machine.html#a93d66cb2a89b186789d655a08b02674e", null ],
    [ "changeState", "class_state_machine.html#abfbff45222937a89d9506c15c215ad76", null ],
    [ "setState", "class_state_machine.html#a7ade51d8e6559ad00c41285a518cb7cc", null ],
    [ "currState", "class_state_machine.html#a324e7c530fcbd09e7dcb9cd88f701859", null ]
];