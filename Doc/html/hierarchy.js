var hierarchy =
[
    [ "album_t", "structalbum__t.html", null ],
    [ "Animated", "class_animated.html", [
      [ "AnimationTest", "class_animation_test.html", null ]
    ] ],
    [ "BitmapFont", "class_bitmap_font.html", null ],
    [ "Card", "class_card.html", null ],
    [ "CardInterface", "class_card_interface.html", null ],
    [ "Deck", "class_deck.html", null ],
    [ "Dialogue", "struct_dialogue.html", null ],
    [ "DialogueInterface", "class_dialogue_interface.html", null ],
    [ "DialogueManager", "class_dialogue_manager.html", null ],
    [ "Drawable", null, [
      [ "Character", "class_character.html", [
        [ "NPC", "class_n_p_c.html", null ],
        [ "Player", "class_player.html", null ]
      ] ],
      [ "Map", "class_map.html", [
        [ "TiledMap", "class_tiled_map.html", null ]
      ] ]
    ] ],
    [ "Game", "class_game.html", null ],
    [ "BitmapFont::limit_info", "struct_bitmap_font_1_1limit__info.html", null ],
    [ "Log", "class_log.html", null ],
    [ "MapInterface", "class_map_interface.html", null ],
    [ "SavingInterface", "class_saving_interface.html", null ],
    [ "SoundPlayer", "class_sound_player.html", null ],
    [ "SpriteSheet", "class_sprite_sheet.html", null ],
    [ "State", "class_state.html", [
      [ "GameState", "class_game_state.html", null ],
      [ "IntroductionState", "class_introduction_state.html", null ]
    ] ],
    [ "StateMachine", "class_state_machine.html", null ],
    [ "Tile", "struct_tile.html", null ]
];