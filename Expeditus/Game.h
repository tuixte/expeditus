/*
 * Game.h
 * Project: Expeditus
 * Author: Michele Marolla (mic.marolla@gmail.com)
 */

#ifndef _GAME_H_
#define _GAME_H_

#include <SFML\Window.hpp>
#include "StateMachine.h"

/*! \brief Main class, contains game's loop, */
class Game{

private:
	sf::Clock _fpsClock;		//!< Used to cap or get frame-per-second value. \sa EXPEDITUS_FPS_CAPPING_ENABLED
	sf::RenderWindow _window;	//!< Game's window
	StateMachine _machine;		//!< Game's state machine.

public:
	/*! \brief Default constructor */
	Game();

	/*! \brief Game main loop.
	 *
	 *	FPS capping is done here, if EXPEDITUS_FPS_CAPPING_ENABLED is defined.
	 *  \sa Consts.h
	 */
	void run();

};

#endif