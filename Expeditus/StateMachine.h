/*
 * StateMachine.h
 * Project: Expeditus
 * Author: Michele Marolla (mic.marolla@gmail.com)
 */

#ifndef _STATE_MACHINE_H_
#define _STATE_MACHINE_H_

#include "State.h"
#include <SFML\Graphics.hpp>

/*! \brief Simple state machine. Handles game's states.
 *	\sa State States.h
 */
class StateMachine{

private:
	sf::RenderWindow *_window; //!< Pointer to game's window
	State *_previousState; //!< Last used state

public:
	//! Enumeration of all the possible states
	enum states{
		previous,
		introduction,
		game
	};

	/*! \brief Current game state.
	 *	Creation and destruction of states is handled by StateMachine::changeState().
	 *	\sa changeState()
	 */
	State *currState;

public:
	/*! \brief Constructor.
	 *	\param[in] window Pointer to game's render window.
	 */
	StateMachine(sf::RenderWindow *window);

	/*! \brief Destructor. */
	~StateMachine();

	/*! \brief Change game state
	 *
	 *	This function cleans up the previous state.
	 *	\param[in] nextState states::state_t enum value corresponding to the next game state.
			For example, pass states::state_t::introduction to go on introduction state.
	 *	\sa setState() states States.h
	 */
	void changeState(states nextState);

	/*! \brief Change game state
	*
	*	This function DOES NOT clean up the previous state.
	*	Use it for menus, for example.
	*	\param[in] nextState states::state_t enum value corresponding to the next game state.
	For example, pass states::state_t::introduction to go on introduction state.
	*	\sa changeState() states States.h
	*/
	void setState(states nextState);
};

#endif