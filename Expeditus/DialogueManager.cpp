/*
* DialogueManager.cpp
* Project: Expeditus
* Author: Michele Marolla (mic.marolla@gmail.com)
*/

#include "DialogueManager.h"
#include <string>
#include <SFML\Graphics.hpp>
#include "BitmapFont.h"
#include "Consts.h"
#include "Dialogue.h"
#include "Log.h"

DialogueManager::DialogueManager(sf::RenderWindow *window){
	this->_window = window;
	this->_working = this->_series = this->_delaying = this->_divided = this->_done = this->_answerGiven = false;
	this->_limitStart = 0;
	this->_limitEnd = -1;
	this->_nextLimitStart = -1;
	this->_drawCursor = true;
	this->_cursorOnYes = true;
}

void DialogueManager::init(std::string filepath){
	if (this->_dialogueInterface.isOpen())
		this->_dialogueInterface.close();
	this->_dialogueInterface.open(filepath);
	this->_font.load("res/font_black.png", sf::Color(255, 255, 255, 0));
	this->_font.setLimit(EXPEDITUS_DIALOGUE_POS_X, EXPEDITUS_DIALOGUE_POS_Y, EXPEDITUS_DIALOGUE_POS_LIMIT_X, EXPEDITUS_DIALOGUE_POS_LIMIT_Y);
	this->_sheet.load("res/dialRect.png");
	this->_sheet.get(&this->_cursor, 0, 49, 16, 10);
	this->_cursor.setPosition(EXPEDITUS_DIALOGUE_CURSOR_X, EXPEDITUS_DIALOGUE_CURSOR_Y);
	this->_sheet.get(&this->_answerCursor, 17, 49, 9, 10);
}

void DialogueManager::events(){
	if (!this->_working)
		return;
	sf::Event event;
	while (this->_window->pollEvent(event)){
		if (event.type == sf::Event::KeyPressed){
			if (event.key.code == sf::Keyboard::Key::Space){
				if (this->_done && this->_dialogue.answer && !this->_answerGiven)
					this->_answerGiven = true;

				if (!this->_dialogue.answer || !this->_answerGiven){
					this->_font.stopDelay();
					if (!this->_font.delayCompleted() && this->_delaying){
						this->_delaying = false;
					}else{
						if (this->_divided && this->_limitEnd < this->_dialogue.text.length()){
							this->_delaying = true;
							this->_limitStart = (this->_nextLimitStart != -1) ? this->_nextLimitStart : this->_limitEnd;
							int index = this->_font.exceedY(this->_dialogue.text.substr(this->_limitStart), EXPEDITUS_DIALOGUE_POS_X, EXPEDITUS_DIALOGUE_POS_Y);
							if (index != -1)
								this->_limitEnd += index;
							else
								this->_limitEnd = this->_dialogue.text.length();
						}else if (!this->_done){
							this->_done = true;
						}
					}
				}
				if (this->_done && (!this->_dialogue.answer || this->_answerGiven)){
					if (this->_dialogue.next_uid == 0 || !this->_series){
						this->_working = this->_series = this->_delaying = this->_divided = false;
					}else if (this->_series){
						this->alone(this->_dialogue.next_uid);
					}
				}
			}else if (event.key.code == sf::Keyboard::Key::Up || event.key.code == sf::Keyboard::Key::Down){
				if (this->_dialogue.answer && this->_done)
					this->_cursorOnYes = !this->_cursorOnYes;
			}
		}else if (event.type == sf::Event::Closed){
			this->_window->close();
		}
	}
}

void DialogueManager::update(){
	if (this->_working){
		if(!this->_done && this->_delaying && !this->_font.delayStarted()){
			if (this->_divided){
				if (this->_limitEnd < this->_dialogue.text.length()){ // Text is yet too long
					/* Look for the first space or newline character, if not found, truncate the text */
					for (int i = this->_limitEnd; i >= this->_limitStart; i--){
						if (this->_dialogue.text[i] == ' ' || this->_dialogue.text[i] == '\n'){ // Found!
							this->_limitEnd = i;
							this->_nextLimitStart = i + 1;
							break;
						}
					}
				}
				this->_font.startDelay(this->_window, this->_dialogue.text.substr(this->_limitStart, this->_limitEnd - this->_limitStart), EXPEDITUS_DIALOGUE_DELAY);
			}else{
				this->_font.startDelay(this->_window, this->_dialogue.text.substr(this->_limitStart), EXPEDITUS_DIALOGUE_DELAY);
			}
		}
	}
}

void DialogueManager::draw(){
	if (this->_working){
		this->_drawRectangle();

		/* Print text */
		sf::Vector2i pos(EXPEDITUS_DIALOGUE_POS_X, EXPEDITUS_DIALOGUE_POS_Y);
		if (!this->_divided || (this->_limitStart == 0 && this->_limitEnd != -1))
			if (this->_dialogue.character.length() > 0)
				pos = this->_font.print(this->_window, this->_dialogue.character + ": ", pos.x, pos.y);
		
		if (this->_font.delayStarted()){
			this->_font.printWithDelay(pos.x, pos.y);
		}else{
			if (this->_divided)
				this->_font.print(this->_window, this->_dialogue.text.substr(this->_limitStart, this->_limitEnd - this->_limitStart), pos.x, pos.y);
			else
				this->_font.print(this->_window, this->_dialogue.text, pos.x, pos.y);
		}
	}
}

void DialogueManager::cleanup(){
	if (this->_dialogueInterface.isOpen())
		this->_dialogueInterface.close();
}

void DialogueManager::start(int uid){
	this->_dialogue = this->_dialogueInterface.get(uid);
	this->start(&this->_dialogue);
}

void DialogueManager::start(Dialogue *dialogue){
	this->_series = true;
	if (dialogue != &this->_dialogue)
		this->_dialogue = *dialogue;
	this->alone(&this->_dialogue);
}

void DialogueManager::alone(int uid){
	this->_dialogue = this->_dialogueInterface.get(uid);
	this->alone(&this->_dialogue);
}

void DialogueManager::alone(Dialogue *dialogue){
	if (dialogue != &this->_dialogue)
		this->_dialogue = *dialogue;
	this->_working = this->_delaying = true;
	this->_done = false;
	this->_cursorOnYes = true;
	int index = EXPEDITUS_DIALOGUE_POS_X;
	if (this->_dialogue.character.length() > 0)
		index = this->_font.exceedY(this->_dialogue.character + ": ", index, EXPEDITUS_DIALOGUE_POS_Y);
	index = this->_font.exceedY(this->_dialogue.text, index, EXPEDITUS_DIALOGUE_POS_Y);
	this->_limitStart = 0;
	if (index != -1){ // Text is too long
		this->_divided = true;
		this->_limitEnd = index - this->_dialogue.character.length() - 2; // Remove character name length and ": " length
		while (this->_dialogue.text[this->_limitEnd] == ' ' || this->_dialogue.text[this->_limitEnd] == '\n')
			this->_limitEnd--;
		this->_nextLimitStart = -1;
	}else{
		this->_divided = false;
	}
	this->_cursorClock.restart();
}

void DialogueManager::_drawRectangle(){
	sf::Sprite sprite;

	/* Dialogue rectangle */
	// Up-left corner
	this->_sheet.get(&sprite, 0, 0, 15, 15);
	sprite.setPosition(EXPEDITUS_DIALOGUE_RECT_LEFT, EXPEDITUS_DIALOGUE_RECT_TOP);
	this->_window->draw(sprite);

	// Up-right corner
	this->_sheet.get(&sprite, 16, 0, 15, 15);
	sprite.setPosition(EXPEDITUS_DIALOGUE_RECT_RIGHT, EXPEDITUS_DIALOGUE_RECT_TOP);
	this->_window->draw(sprite);

	// Bottom-left corner
	this->_sheet.get(&sprite, 0, 16, 15, 15);
	sprite.setPosition(EXPEDITUS_DIALOGUE_RECT_LEFT, EXPEDITUS_DIALOGUE_RECT_BOTTOM);
	this->_window->draw(sprite);

	// Bottom-right corner
	this->_sheet.get(&sprite, 16, 16, 15, 15);
	sprite.setPosition(EXPEDITUS_DIALOGUE_RECT_RIGHT, EXPEDITUS_DIALOGUE_RECT_BOTTOM);
	this->_window->draw(sprite);

	// Upper side
	this->_sheet.get(&sprite, 32, 0, 16, 15);
	for (int x = EXPEDITUS_DIALOGUE_RECT_LEFT + 15; x + 16 <= EXPEDITUS_DIALOGUE_RECT_RIGHT; x += 16){
		sprite.setPosition(x, EXPEDITUS_DIALOGUE_RECT_TOP);
		this->_window->draw(sprite);
	}

	// Left side
	this->_sheet.get(&sprite, 17, 32, 15, 16);
	for (int y = EXPEDITUS_DIALOGUE_RECT_TOP + 15; y + 16 <= EXPEDITUS_DIALOGUE_RECT_BOTTOM + 3; y += 16){
		sprite.setPosition(EXPEDITUS_DIALOGUE_RECT_LEFT, y);
		this->_window->draw(sprite);
	}
	
	// Bottom side
	this->_sheet.get(&sprite, 32, 16, 16, 15);
	for (int x = EXPEDITUS_DIALOGUE_RECT_LEFT + 15; x + 16 <= EXPEDITUS_DIALOGUE_RECT_RIGHT; x += 16){
		sprite.setPosition(x, EXPEDITUS_DIALOGUE_RECT_BOTTOM);
		this->_window->draw(sprite);
	}
	
	// Right side
	this->_sheet.get(&sprite, 33, 32, 15, 16);
	for (int y = EXPEDITUS_DIALOGUE_RECT_TOP + 15; y + 16 <= EXPEDITUS_DIALOGUE_RECT_BOTTOM + 3; y += 16){
		sprite.setPosition(EXPEDITUS_DIALOGUE_RECT_RIGHT, y);
		this->_window->draw(sprite);
	}

	// Internal
	this->_sheet.get(&sprite, 0, 32, 16, 16);
	for (int x = EXPEDITUS_DIALOGUE_RECT_LEFT + 15; x + 16 <= EXPEDITUS_DIALOGUE_RECT_RIGHT; x += 16){
		for (int y = EXPEDITUS_DIALOGUE_RECT_TOP + 15; y + 16 <= EXPEDITUS_DIALOGUE_RECT_BOTTOM + 3; y += 16){
			sprite.setPosition(x, y);
			this->_window->draw(sprite);
		}
	}

	/* Answer's rectangle */
	if (this->_dialogue.answer && this->_done){
		// Up-left corner
		this->_sheet.get(&sprite, 0, 0, 15, 15);
		sprite.setPosition(EXPEDITUS_DIALOGUE_A_RECT_LEFT, EXPEDITUS_DIALOGUE_A_RECT_TOP);
		this->_window->draw(sprite);

		// Up-right corner
		this->_sheet.get(&sprite, 16, 0, 15, 15);
		sprite.setPosition(EXPEDITUS_DIALOGUE_A_RECT_RIGHT, EXPEDITUS_DIALOGUE_A_RECT_TOP);
		this->_window->draw(sprite);

		// Bottom-left corner
		this->_sheet.get(&sprite, 0, 16, 15, 15);
		sprite.setPosition(EXPEDITUS_DIALOGUE_A_RECT_LEFT, EXPEDITUS_DIALOGUE_A_RECT_BOTTOM);
		this->_window->draw(sprite);

		// Bottom-right corner
		this->_sheet.get(&sprite, 16, 16, 15, 15);
		sprite.setPosition(EXPEDITUS_DIALOGUE_A_RECT_RIGHT, EXPEDITUS_DIALOGUE_A_RECT_BOTTOM);
		this->_window->draw(sprite);

		// Upper side
		this->_sheet.get(&sprite, 32, 0, 16, 15);
		for (int x = EXPEDITUS_DIALOGUE_A_RECT_LEFT + 15; x + 16 <= EXPEDITUS_DIALOGUE_A_RECT_RIGHT; x += 16){
			sprite.setPosition(x, EXPEDITUS_DIALOGUE_A_RECT_TOP);
			this->_window->draw(sprite);
		}

		// Left side
		this->_sheet.get(&sprite, 17, 32, 15, 16);
		for (int y = EXPEDITUS_DIALOGUE_A_RECT_TOP + 15; y + 16 <= EXPEDITUS_DIALOGUE_A_RECT_BOTTOM + 3; y += 16){
			sprite.setPosition(EXPEDITUS_DIALOGUE_A_RECT_LEFT, y);
			this->_window->draw(sprite);
		}

		// Bottom side
		this->_sheet.get(&sprite, 32, 16, 16, 15);
		for (int x = EXPEDITUS_DIALOGUE_A_RECT_LEFT + 15; x + 16 <= EXPEDITUS_DIALOGUE_A_RECT_RIGHT; x += 16){
			sprite.setPosition(x, EXPEDITUS_DIALOGUE_A_RECT_BOTTOM);
			this->_window->draw(sprite);
		}

		// Right side
		this->_sheet.get(&sprite, 33, 32, 15, 16);
		for (int y = EXPEDITUS_DIALOGUE_A_RECT_TOP + 15; y + 16 <= EXPEDITUS_DIALOGUE_A_RECT_BOTTOM + 3; y += 16){
			sprite.setPosition(EXPEDITUS_DIALOGUE_A_RECT_RIGHT, y);
			this->_window->draw(sprite);
		}

		// Internal
		this->_sheet.get(&sprite, 0, 32, 16, 16);
		for (int x = EXPEDITUS_DIALOGUE_A_RECT_LEFT + 15; x + 16 <= EXPEDITUS_DIALOGUE_A_RECT_RIGHT; x += 16){
			for (int y = EXPEDITUS_DIALOGUE_A_RECT_TOP + 15; y + 16 <= EXPEDITUS_DIALOGUE_A_RECT_BOTTOM + 3; y += 16){
				sprite.setPosition(x, y);
				this->_window->draw(sprite);
			}
		}

		// Text
		this->_font.print(this->_window, "YES", EXPEDITUS_DIALOGUE_A_POS_X, EXPEDITUS_DIALOGUE_A_POS_Y1);
		this->_font.print(this->_window, "NO", EXPEDITUS_DIALOGUE_A_POS_X, EXPEDITUS_DIALOGUE_A_POS_Y2);

		// Cursor
		this->_sheet.get(&sprite, 17, 49, 9, 10);
		if (this->_cursorOnYes)
			sprite.setPosition(EXPEDITUS_DIALOGUE_A_CURSOR_X, EXPEDITUS_DIALOGUE_A_CURSOR_Y1);
		else
			sprite.setPosition(EXPEDITUS_DIALOGUE_A_CURSOR_X, EXPEDITUS_DIALOGUE_A_CURSOR_Y2);
		this->_window->draw(sprite);
	}

	/* Cursor */
	if (this->_cursorClock.getElapsedTime().asMilliseconds() >= EXPEDITUS_DIALOGUE_CURSOR_DELAY){
		this->_drawCursor = !this->_drawCursor;
		this->_cursorClock.restart();
	}
	if (this->_drawCursor)
		this->_window->draw(this->_cursor);
}