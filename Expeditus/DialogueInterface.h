/*
 * DialogueInterface.h
 * Project: Expeditus
 * Author: Michele Marolla (mic.marolla@gmail.com)
 */

#ifndef _DIALOGUE_INTERFACE_H_
#define _DIALOGUE_INTERFACE_H_

#include <fstream>
#include "Dialogue.h"

/*! \brief Interface to read dialogues from binary file.
 *
 * Dialogue syntax:
 * UID Character_name Text_size Text Answer(yes/no) Script(yes/no)
 */
class DialogueInterface{

private:
	std::fstream _file; //!< Dialogue file

public:
	/*!	\brief Open dialogue binary file.
	*	\param[in] path Dialogue's file path.
	*	\sa isOpen() close()
	*/
	void open(std::string path);

	/*! \brief Check if any dialogue file has been opened.
	 *	\return True if any dialogue file has been opened, false else.
	 *	\sa open() close()
	 */
	inline bool isOpen() const { return this->_file.is_open(); }

	/*! \brief Close dialogue file.
	 *	\sa open() isOpen()
	 */
	void close();

	/*! \brief Read dialogue with given uid from file.
	 *	\param[in] uid UID of wanted dialogue.
	 *	\return Wanted dialogue; if not found, an empty one (uid: 0)
	 */
	Dialogue get(int uid);

};

#endif