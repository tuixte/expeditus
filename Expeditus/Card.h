/*
 * Card.h
 * Project: Expeditus
 * Author: Michele Marolla (mic.marola@gmail.com)
 */

#ifndef _CARD_H_
#define _CARD_H_

#include <string>

/*!	\brief Basic structure of a card.
 *	\sa CardInterface
 */
class Card{

public:
	int uid;		//!< Card'uid (default: 0) */
	int limit;	//!< How many copy of this card characters can own
	std::string name;		//!< Card's name
	std::string type;		//!< Card's type (battle, object, utility, special)
	std::string desc;		//!< Card's description

	//! Constructor
	Card();

	//! Delete all card information.
	void clear();
};

#endif