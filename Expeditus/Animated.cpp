/*
* Animated.cpp
* Project: Expeditus
* Author: Michele Marolla (mic.marolla@gmail.com)
*/

#include "Animated.h"
#include <SFML\Graphics.hpp>

Animated::Animated(int frames){
	this->frameRects = new sf::IntRect[frames];
}

Animated::~Animated(){
	delete[] this->frameRects;
}