/*
* SavingInterface.cpp
* Project: Expeditus
* Author: Michele Marolla (mic.marolla@gmail.com)
*/

#include "SavingInterface.h"
#include <cstdlib>
#include <cstring>
#include "Player.h"
#include "Consts.h"
#include "Log.h"

void SavingInterface::save(Player *player){
	Log::get() << "Saving on '" << EXPEDITUS_FILE_SAVING << "'...";
	this->_file.open(EXPEDITUS_FILE_SAVING, std::ios::out | std::ios::binary | std::ios::trunc);
	if (this->_file.fail()){
		Log::get().flag(Log::flags::critic);
		Log::get().newline();  Log::get() << "Error on opening " << EXPEDITUS_FILE_SAVING << "file. Exit."; Log::get().newline();
		exit(1);
	}
	char name[EXPEDITUS_CHARACTER_NAME_SIZE];
	int size = player->name.length();
	strcpy_s(name, EXPEDITUS_CHARACTER_NAME_SIZE, player->name.c_str());
	name[size] = '\0'; size++; // Adding null-terminating character
	this->_file.write((char*)&size, sizeof(int));
	this->_file.write(name, size);
	this->_file.close();
	Log::get() << "done."; Log::get().newline();
}

void SavingInterface::load(Player *player){
	Log::get() << "Loading from '" << EXPEDITUS_FILE_SAVING << "'...";
	this->_file.open(EXPEDITUS_FILE_SAVING, std::ios::in | std::ios::binary);
	if (this->_file.fail()){
		Log::get().flag(Log::flags::critic);
		Log::get().newline();  Log::get() << "Error on opening " << EXPEDITUS_FILE_SAVING << "file. Exit."; Log::get().newline();
		exit(1);
	}
	int size;
	char name[EXPEDITUS_CHARACTER_NAME_SIZE];
	this->_file.seekg(0);
	this->_file.read((char*)&size, sizeof(int));
	this->_file.read(name, size);
	player->name.assign(name);
	this->_file.close();

	Log::get() << "done."; Log::get().newline();
}