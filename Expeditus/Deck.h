/*
 * Deck.h
 * Project: Expeditus
 * Author: Michele Marolla (mic.marolla@gmail.com)
 */

#include "Card.h"
#include "Consts.h"

/* \brief Album of cards
*
* The value album_t::data[i] contains how many times the character owns
* the card with uid i.
*/
typedef struct album_t{
	int data[EXPEDITUS_CARDS_NUMBER];
	album_t(){
		for (int i = 0; i < EXPEDITUS_CARDS_NUMBER; i++)
			data[i] = 0;
	}
} album_t;

/*!
 * \brief Class that handles a deck of cards.
 * \sa Card
 */
class Deck{

private:
	/*! \brief Cards in the deck.
	 *	\sa EXPEDITUS_DECK_MAX_SIZE
	 */
	Card _deck[EXPEDITUS_DECK_MAX_SIZE];
	int _count; //!< Number of cards in the deck

public:
	//! Constructor
	Deck();

	/*! \brief Get a card from the deck searching by its uid.
	 *	\param[in] uid UID of wanted card.
	 *	\return Pointer to wanted card if found; null pointer else.
	 *	\sa getCardByPos()
	 */
	Card* getCardByUID(int uid);

	/*! \brief Get a card from the deck searching by its position in the array.
	*
	*	Card's position starts from 0.
	*	\param[in] pos Position of wanted card (first card's position is 0).
	*	\return Pointer to wanted card if found; null pointer if pos is out of range.
	*	\sa getCardByUID()
	*/
	Card* getCardByPos(int pos);

	/*! \brief Add a new card into the deck.
	 *
	 * The content of the card is copied into the array.
	 * \param[in] card Card to insert in the deck.
	 * \return Position in the array of the card (first card's position is 0).
	 */
	int addCard(Card &card);

	/*! \brief Remove card from deck searching by its uid.
	 *
	 *	If the card is not found, does nothing.
	 *	This function modifies the other cards's position: in particular, the last card in the deck is moved
	 *	into the space of the removed card.
	 *	\param[in] uid UID of the card to remove.
	 *	\sa delCardByPos()
	 */
	void delCardByUID(int uid);

	/*! \brief Remove card from deck searching by its position in the array.
	*
	*	If the card is not found, does nothing.
	*	This function modifies the other cards's position: in particular, the last card in the deck is moved
	*	into the space of the removed card.
	*	\param[in] pos Position of wanted card (first card's position is 0).
	*	\sa delCardByUID()
	*/
	void delCardByPos(int pos);

	/*! \brief Get the number of cards stored in the deck.
	 *	\return Number of cards in the deck.
	 */
	inline int count() const{ return this->_count; }

};