/*
* main.cpp
* Project: Expeditus
* Author: Michele Marolla (mic.marolla@gmail.com)
*/

#include "Game.h"

int main(){
	Game game;
	game.run();
	return 0;
}