/*
 * SoundPlayer.h
 * Project: Expeditus
 * Author: Michele Marolla (mic.marolla@gmail.com)
 */

#ifndef _SOUND_PLAYER_H_
#define _SOUND_PLAYER_H_

#include <string>
#include <SFML\Audio.hpp>

/*! Class to load and play sounds (not intended for long music). */
class SoundPlayer{

private:
	bool _ready;				//!< True if any sound has been loaded, false else. Used internally.
	sf::SoundBuffer _buffer;	//!< Buffer that contains sound
	sf::Sound _sound;			//!< Sound object

public:
	//! Constructor
	SoundPlayer();

	/*! \brief Load sound file located
	 *	\param[in] path Sound file's path
	 */
	void load(std::string path);

	/*! \brief Check if any sound file was loaded.
	 *	\return Returns true if any sound file was loaded, false else.
	 *	\sa load()
	 */
	inline bool isReady() const{ return this->_ready; }

	/*! \brief Play loaded sound.
	 *
	 *	If not sound file was loaded, it does nothing.
	 *	\sa load() play(std::string)
	 */
	void play();

	/*! \brief Load and play sound.
	*
	*	\param[in] path Sound file's path
	*	\sa load() play(std::string)
	*/
	void play(std::string path);

};

#endif