/*
 * SavingInterface.h
 * Project: Expeditus
 * Author: Michele Marolla (mic.marolla@gmail.com)
 */

#ifndef _SAVING_INTERFACE_H_
#define _SAVING_INTERFACE_H_

#include <fstream>

extern struct Player;

/*! \brief Interface to save and load player's game from and into a binary file.
 *
 *	Save file's syntax:
 *  Name_size Name_string
 *	\sa Player EXPEDITUS_FILE_SAVING
 */
class SavingInterface{

private:
	std::fstream _file;	//!< Save file

public:
	/*! Write player's information into binary save file.
	 *	\param[in] player Pointer to player instance.
	 *	\sa load()
	 */
	void save(Player *player);

	/*! Read player's information from binary save file.
	*	\param[in] player Pointer to player instance.
	*	\sa save()
	*/
	void load(Player *player);

};

#endif