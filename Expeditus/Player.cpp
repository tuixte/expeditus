/*
* Player.cpp
* Project: Expeditus
* Author: Michele Marolla (mic.marolla@gmail.com)
*/

#include "Player.h"
#include "CardInterface.h"
#include "Consts.h"

Player::Player(int uid) : Character(uid){}

void Player::setName(std::string name){
	this->name = name;
}

bool Player::addCard(int id){
	CardInterface interface;
	int limit = int(interface.get(id).limit);
	if (id > 0 && id <= EXPEDITUS_CARDS_NUMBER){
		if (this->_album.data[id] < limit){
			this->_album.data[id] += 1;
			return true;
		}
	}
	return false;
}

bool Player::removeCard(int id){
	if (id > 0 && id < EXPEDITUS_CARDS_NUMBER){
		if (this->_album.data[id] > 0){
			this->_album.data[id] -= 1;
			return true;
		}
	}
	return false;
}

int Player::owned(int id){
	if (id > 0 && id < EXPEDITUS_CARDS_NUMBER)
		return this->_album.data[id];
	return 0;
}