/*
* GameState.h
* Project: Expeditus
* Author: Michele Marolla (mic.marolla@gmail.com)
*/

#ifndef _GAME_STATE_H_
#define _GAME_STATE_H_

#include <SFML\Graphics.hpp>
#include "CardInterface.h"
#include "DialogueManager.h"
#include "Map.h"
#include "NPC.h"
#include "SoundPlayer.h"
#include "SpriteSheet.h"
#include "State.h"
#include "StateMachine.h"
#include "TiledMap.h"

/*! \brief Main game state. Now, it's a test for game's features. */
class GameState : public State{

private:
	DialogueManager _manager;		//!< Dialogue manager
	NPC _npc;						//!< Game's npc
	Player _player;					//!< Player
	TiledMap _map;					//!< Tiled map.
	bool _collision;				//!< Player - NPC collision flag
	bool _cardGiven;				//!< Card given to the player by NPC flag
	sf::Sprite _card;				//!< Received card's sprite
	SpriteSheet _cardSheet;			//!< Cards's sprite sheet
	bool _showCard;					//!< True if card has to be shown, false else
	SoundPlayer _sound;				//!< Sound player
	CardInterface _cardInterface;	//!< Cards's interface

public:
	/*! \brief Constructor
	*	\param[in] machine Pointer to game's state machine.
	*	\param[in] window Pointer to game's render window.
	*/
	GameState(StateMachine *machine, sf::RenderWindow *window);

	/*! \brief Initialize resources */
	void init();

	/*! \brief Handle events */
	void events();

	/*! \brief Update state */
	void update();

	/*! \brief Draw state */
	void draw();

	/*! \brief Release resources */
	void cleanup();

};

#endif