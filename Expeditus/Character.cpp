/*
* Character.cpp
* Project: Expeditus
* Author: Michele Marolla (mic.marolla@gmail.com)
*/

#include "Character.h"
#include <string>
#include <SFML\Graphics.hpp>
#include "Log.h"
#include "TiledMap.h"
#include "SpriteSheet.h"

Character::Character(int _uid) : uid(_uid){
	this->map = 0;
	this->_offX = this->_offY = 0;
	this->_velocity = -1;
}

void Character::load(std::string filepath){
	Log::get() << "Loading texture from '" << filepath << "'...";
	if (!this->_texture.loadFromFile(filepath)){
		Log::get().newline(); Log::get().log("Unable to load texture", Log::flags::error); Log::get().newline();
	}
	this->_sprite.setTexture(this->_texture);
	Log::get() << "done."; Log::get().newline();
}

void Character::getFromSheet(SpriteSheet *sheet, int x, int y, int w, int h){
	sheet->get(&this->_sprite, x, y, w, h);
}

sf::Vector2f Character::getPosition() const{
	sf::Vector2f pos = this->_sprite.getPosition();
	return sf::Vector2f(pos.x, pos.y);
}

sf::Vector2u Character::getSize() const{
	sf::IntRect size = this->_sprite.getTextureRect();
	return sf::Vector2u(size.width, size.height);
}

void Character::setPosition(float x, float y){
	this->_sprite.setPosition(x, y);
}

void Character::setPosition(sf::Vector2f pos){
	this->_sprite.setPosition(pos);
}

void Character::setPixelVelocity(float velocity){
	if (velocity >= 0)
		this->_velocity = velocity;
}

void Character::setTileVelocity(float velocity){
	if (velocity >= 0)
		this->_velocity = velocity * EXPEDITUS_MAP_TILE_SIZE;
}

void Character::move(float offX, float offY){
	this->_offX = offX;
	this->_offY = offY;
}

void Character::moveTiles(float offX, float offY){
	this->_offX = offX * EXPEDITUS_MAP_TILE_SIZE;
	this->_offY = offY * EXPEDITUS_MAP_TILE_SIZE;
}

bool Character::isNear(Character *other){
	// Check if character touches with one of its edges the other characters
	sf::FloatRect rect = this->_sprite.getGlobalBounds();
	rect.left -= 1; rect.width += 2; rect.top -= 1; rect.height += 2;
	return rect.intersects(other->_sprite.getGlobalBounds());
}

bool Character::collides(Character *other){
	return this->_sprite.getGlobalBounds().intersects(other->_sprite.getGlobalBounds());
}

void Character::update(float s){
	float offX = 0, offY = 0;

	/* X offset */
	if (this->_offX > 0){
		offX = this->_velocity * s;
		if (offX > this->_offX)
			offX = this->_offX;
	}else if (this->_offX < 0){
		offX = this->_velocity * s * -1;
		if (offX < this->_offX)
			offX = this->_offX;
	}
	this->_offX -= offX;
	/* Y offset */
	if (this->_offY > 0){
		offY = this->_velocity * s;
		if (offY > this->_offY)
			offY = this->_offY;
	}
	else if (this->_offY < 0){
		offY = this->_velocity * s * -1;
		if (offY < this->_offY)
			offY = this->_offY;
	}
	this->_offY -= offY;
	
	// Move it!
	if (offX != 0 || offY != 0){
		if (!this->map){
			this->_sprite.move(offX, offY);
			this->_offX = this->_offY = 0;
		}else{
			if (this->map->move(this, offX, offY))
				this->_sprite.move(offX, offY);
			else
				this->_offX = this->_offY = 0;
		}
	}
}

void Character::draw(sf::RenderTarget &target, sf::RenderStates states) const{
	target.draw(this->_sprite, states);
}