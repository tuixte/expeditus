/*
* IntroductionState.cpp
* Project: Expeditus
* Author: Michele Marolla (mic.marolla@gmail.com)
*/

#include <cstdlib>
#include "AnimationTest.h"
#include "IntroductionState.h"
#include "State.h"
#include "StateMachine.h"
#include <SFML\Graphics.hpp>

IntroductionState::IntroductionState(StateMachine *machine, sf::RenderWindow *window) : State(machine, window), _manager(window), animTest(window){}

void IntroductionState::init(){
	this->_manager.init("res/dialogues_intro.bin");
	this->_manager.start(1);
}

void IntroductionState::events(){
	if (this->_paused) return;

	this->_manager.events();
	sf::Event event;
	while (this->_window->pollEvent(event))
		if (event.type == sf::Event::Closed)
			this->_window->close();
}

void IntroductionState::update(){
	if (this->_paused) return;

	this->_manager.update();
	if (this->_manager.answerGiven()){
		bool answer = this->_manager.getAnswer();
		if (answer)
			this->_machine->changeState(StateMachine::states::game);
		else
			exit(0);
	}
}

void IntroductionState::draw(){
	if (this->_paused) return;

	this->_window->clear(sf::Color::Yellow);
	this->_manager.draw();
	this->animTest.draw();
	this->_window->display();
}

void IntroductionState::cleanup(){
	this->_manager.cleanup();
}