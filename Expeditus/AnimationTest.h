#ifndef _ANIMATION_TEST_H_
#define _ANIMATION_TEST_H_

#include "Animated.h"
#include <SFML\Graphics.hpp>

class AnimationTest : public Animated{

private:
	sf::Texture _texture;
	sf::RenderWindow *_window;
	sf::Sprite _sprite;
	
public:
	AnimationTest(sf::RenderWindow *window);
	void draw();

};

#endif