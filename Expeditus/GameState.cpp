/*
* GameState.cpp
* Project: Expeditus
* Author: Michele Marolla (mic.marolla@gmail.com)
*/

#include "GameState.h"
#include "State.h"
#include "StateMachine.h"
#include <SFML\Graphics.hpp>

GameState::GameState(StateMachine *machine, sf::RenderWindow *window)
	: State(machine, window), _player(1), _npc(2), _manager(window){
	this->_collision = false;
	this->_cardGiven = false;
	this->_showCard = false;
}

void GameState::init(){
	if (this->_paused) return;

	this->_cardInterface.open();
	this->_sound.load("res/item_get.wav");
	this->_cardSheet.load("res/cards.png");
	this->_cardSheet.get(&this->_card, 120, 0, 60, 88);
	sf::Vector2u winSize = this->_window->getSize();
	this->_card.setPosition(winSize.x/2-30, winSize.y/2-22);
	this->_manager.init("res/dialogues_demo.bin");
	this->_player.load("res/player.png");
	this->_npc.load("res/npc.png");
	this->_map.loadTexture("res/background.png");
	this->_map.loadMap("res/map_1.bin");
	this->_map.addPlayer(&this->_player);
	this->_map.addCharacter(&this->_npc);
	this->_player.setPosition(this->_map.getPlayerDefaultPosition());
	this->_player.setTileVelocity(5);
	this->_npc.setPosition(12 * EXPEDITUS_MAP_TILE_SIZE, 3 * EXPEDITUS_MAP_TILE_SIZE);
	this->_manager.alone(1);
}

void GameState::events(){
	if (this->_paused) return;

	sf::Event event;
	this->_manager.events();
	while (this->_window->pollEvent(event)){
		if (event.type == sf::Event::Closed){
			this->_window->close();
		}else if(event.type == sf::Event::KeyPressed){
			if (sf::Keyboard::isKeyPressed(sf::Keyboard::Up))
				this->_player.moveTiles(0, -1);
			if (sf::Keyboard::isKeyPressed(sf::Keyboard::Down))
				this->_player.moveTiles(0, +1);
			if (sf::Keyboard::isKeyPressed(sf::Keyboard::Left))
				this->_player.moveTiles(-1, 0);
			if (sf::Keyboard::isKeyPressed(sf::Keyboard::Right))
				this->_player.moveTiles(+1, 0);
		}
	}
}

void GameState::update(){
	if (this->_paused) return;

	this->_map.update();
	this->_manager.update();

	if (this->_showCard && this->_manager.lastUID() == 0 && !this->_manager.working())
		this->_showCard = false;

	/* Collision */
	if (this->_player.isNear(&this->_npc)){
		if (!this->_collision){
			this->_collision = true;
			this->_manager.alone(2);
			this->_cardGiven = true;
		}else{
			if (this->_cardGiven && !this->_manager.working()){
				this->_manager.cleanup();
				this->_manager.init("res/dialogues_misc.bin");
				if (this->_player.addCard(1)){
					this->_sound.play();
					this->_manager.alone(1);
					this->_showCard = true;
				}else{
					this->_manager.alone(2);
				}
				this->_cardGiven = false;
				this->_manager.cleanup();
				this->_manager.init("res/dialogues_demo.bin");
			}
			if (this->_showCard && !this->_manager.working()){
				Card myCard = this->_cardInterface.get(1);
				Dialogue myDial; // uid = 0
				myDial.text = "Card name: " + myCard.name + "\nDescription: " + myCard.desc;
				this->_manager.alone(&myDial);
			}
		}
	}else{
		this->_collision = false;
	}
}

void GameState::draw(){
	if (this->_paused) return;

	this->_window->clear();
	this->_window->draw(this->_map);
	this->_window->draw(this->_player);
	this->_window->draw(this->_npc);
	if (this->_showCard)
		this->_window->draw(this->_card);
	this->_manager.draw();
	this->_map.restartClock();
	this->_window->display();
}

void GameState::cleanup(){
	this->_manager.cleanup();
	this->_map.cleanup();
}