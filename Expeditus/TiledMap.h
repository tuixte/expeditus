/*
* TiledMap.h
* Project: Expeditus
* Author: Michele Marolla (mic.marolla@gmail.com)
*/

#ifndef _TILED_MAP_H_
#define _TILED_MAP_H_

#include <string>
#include <vector>
#include <SFML\Graphics.hpp>
#include "Character.h"
#include "Consts.h"
#include "Map.h"
#include "NPC.h"
#include "Player.h"

//! \brief Simple structure that represents a single map tile.
struct Tile{
	bool movement = true; //!< true if characters can walk on this tile, false else.
	bool event = false;   //!< is true if an event is triggered when player walks on this tile, false else.
};

/*! \brief A map made of more tiles.
 *	\sa Tile
 */
class TiledMap : public Map{

	friend class MapInterface;
protected:
	/*! \brief Map's data
	 *	\sa Tile EXPEDITUS_MAP_MAX_WIDTH EXPEDITUS_MAP_MAX_HEIGHT
	 */
	Tile data[EXPEDITUS_MAP_MAX_WIDTH][EXPEDITUS_MAP_MAX_HEIGHT];

	/*! \brief Map's width (number of tiles).
     * It must be less or equal the EXPEDITUS_MAP_MAX_WIDTH.
  	 * \sa EXPEDITUS_MAP_MAX_WIDTH
	 */
	int _tWidth;

	/*! \brief Map's height (number of tiles).
	 * It must be less or equal the EXPEDITUS_MAP_MAX_HEIGHT.
	 * \sa EXPEDITUS_MAP_MAX_HEIGHT
	 */
	int _tHeight;

	float _playerX;	//!< Default player x position (in pixels)
	float _playerY;	//!< Default player y position (in pixels)

private:
	std::vector<NPC*> _npcs;	//!< Vector containing registered NPCs
	sf::Clock _clock;			//!< Clock used for NPCs's movements
	Player *_player;			//!< Pointer to player entity

public:
	/*! \brief Constructor.	*/
	TiledMap();

	/*! \brief Constructor.
	 *
	 *	It is equivalent than calling TiledMap() default constructor and load() function.
	 *	\param[in] filepath Texture's file path.
	 *	\sa TiledMap() load()
	 */
	TiledMap(std::string filepath);

	/*! \brief Get texture's size (number of tiles).
	 *	\return Texture's width and height (number of tiles).
	 */
	sf::Vector2i getTileSize() const;

	/*! \brief Get information about one tile.
	 *	\param[in] x Tile's x coordinate
	 *	\param[in] y Tile's y coordinate
	 *	\return Wanted tile.
	 *	\sa Tile
	 */
	Tile getTile(int x, int y) const;

	/*!	\brief Get player's default position in the map.
	 *	\return Player's default position.
	 */
	inline sf::Vector2f getPlayerDefaultPosition() const{ return sf::Vector2f(this->_playerX, this->_playerY); }

	/*! \brief Add one character to the map.
	 *	\param[in] character Pointer to the character to add.
	 *	\sa addCharacter(NPC*) addPlayer()
	 */
	void addCharacter(Character *character);

	/*! \brief Add one NPC character to the map.
	*	\param[in] character Pointer to the NPC character to add.
	*	\sa addCharacter(Character*) addPlayer()
	*/
	void addCharacter(NPC *character);

	/*! \brief Add player to the map
	 *
	 *  Use this function only once.
	 *	If you call it more times, only the last player inserted is considered.
	 *	\param[in] player Pointer to the player entity
	 *	\sa addCharacter(Character*) addCharacter(NPC*)
	 */
	void addPlayer(Player *player);

	/*!	\brief Load map information from binary file.
	 *	\param[in] filepath Map binary file's path.
	 */
	void loadMap(std::string filepath);

	/*!	\brief Move one character on the map, if possibile.
	 *
	 *	Character is moved only if movement is possibile in the destination tile.
	 *	\param[in] character Pointer to the character to move
	 *	\param[in] offX X offset (in pixels)
	 *	\param[in] offY Y offset (in pixels)
	 *	\return True if character was successfully moved, false else.
	 */
	bool move(Character *character, float offX, float offY);

	/*! \brief Make registered NPC characters follow them movement patterns, if they have one.
	 */
	void update();

	/*! \brief Restart clock used for NPC movement.
	 *
	 *	Use this function after drawing map.
	 */
	void restartClock();
};

#endif