/*
* DialogueInterface.cpp
* Project: Expeditus
* Author: Michele Marolla (mic.marolla@gmail.com)
*/

#include "DialogueInterface.h"
#include <cstdlib>
#include <string>
#include "Consts.h"
#include "Dialogue.h"
#include "Log.h"

void DialogueInterface::open(std::string path){
	Log::get() << "Opening " << path << " file...";
	this->_file.open(path, std::ios::in | std::ios::binary);
	if (this->_file.fail()){
		Log::get().flag(Log::flags::critic);
		Log::get().newline();  Log::get() << "Error on opening " << path << "file. Exit."; Log::get().newline();
		exit(1);
	}
	Log::get() << "done."; Log::get().newline();
}

void DialogueInterface::close(){
	this->_file.close();
}

Dialogue DialogueInterface::get(int uid){
	Dialogue dialogue;
	
	// Check if file is opened
	if (!this->_file.is_open()){
		Log::get().log("Tried to read a dialogue without opening any file.", Log::flags::error);
		return dialogue;
	}

	// Check input
	if (uid <= 0){
		Log::get().flag(Log::flags::error);
		Log::get() << "Requested an invalid position in binary file (pos < 0)"; Log::get().newline();
		Log::get().flag(Log::flags::text);
		return dialogue;
	}

	// Read file
	char characterName[EXPEDITUS_CHARACTER_NAME_SIZE], text[EXPEDITUS_DIALOGUE_TEXT_MAX_SIZE];
	this->_file.seekg(0);
	int textSize, pos;
	for (int i = 0; i < uid - 1; i++){
		pos = this->_file.tellg();
		this->_file.seekg(pos + EXPEDITUS_UID_SIZE + EXPEDITUS_CHARACTER_NAME_SIZE);
		this->_file.read((char*)&textSize, sizeof(int));
		pos = this->_file.tellg();
		this->_file.seekg(pos + textSize + EXPEDITUS_UID_SIZE + sizeof(bool)*2);
	}
	this->_file.read((char*)&dialogue.uid, EXPEDITUS_UID_SIZE);
	this->_file.read(characterName, EXPEDITUS_CHARACTER_NAME_SIZE);
	dialogue.character.assign(characterName);
	this->_file.read((char*)&textSize, sizeof(int));
	this->_file.read(text, textSize);
	text[textSize] = '\0';
	dialogue.text.assign(text);
	this->_file.read((char*)&dialogue.next_uid, EXPEDITUS_UID_SIZE);
	this->_file.read((char*)&dialogue.answer, sizeof(bool));
	this->_file.read((char*)&dialogue.script, sizeof(bool));

	return dialogue;
}