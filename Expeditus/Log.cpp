/*
* Log.cpp
* Project: Expeditus
* Author: Michele Marolla (mic.marolla@gmail.com)
*/

#include "Log.h"
#include <cstdlib>
#include <ctime>
#include <iostream>
#include <string>
#include "Consts.h"

Log::Log(){
#if EXPEDITUS_LOG_ENABLED == 1
	this->_file.open(EXPEDITUS_FILE_LOG, std::ios::out | std::ios::app);
	if (this->_file.fail()){
		std::cerr << "Error: cannot open '" << EXPEDITUS_FILE_LOG << "' file. Exit." << std::endl;
		exit(1);
	}
	this->_flag = flags::text;
	this->_file << "------------------------------" << std::endl;
	this->_file << "Expeditus started on: ";
	this->printTime();
#endif
}

Log::~Log(){
	this->_file.close();
}

void Log::_header(flags flag){
#if EXPEDITUS_LOG_ENABLED == 1
	if (flag == flags::alert)
		this->_file << "ALERT: ";
	else if (flag == flags::error)
		this->_file << "ERROR: ";
	else if (flag == flags::critic)
		this->_file << "CRITIC: ";
#endif
}

void Log::flag(flags flag){
	this->_flag = flag;
}

void Log::printTime(){
#if EXPEDITUS_LOG_ENABLED == 1
	time_t rawtime = time(NULL);
	localtime_s(&this->_timeinfo, &rawtime);
	this->_file << this->_timeinfo.tm_mday << "/" << (this->_timeinfo.tm_mon+1) << "/" << (this->_timeinfo.tm_year+1900) << " ";
	this->_file << this->_timeinfo.tm_hour << ":" << this->_timeinfo.tm_min << ":" << this->_timeinfo.tm_sec << std::endl;
#endif
}

void Log::newline(){
#if EXPEDITUS_LOG_ENABLED == 1
	this->_file << std::endl;
#endif
}

void Log::log(std::string msg, flags flag){
#if EXPEDITUS_LOG_ENABLED == 1
	this->_header(flag);
	this->_file << msg;
#endif
}

void Log::log(char msg, flags flag){
#if EXPEDITUS_LOG_ENABLED == 1
	this->_header(flag);
	this->_file << msg;
#endif
}

void Log::log(int msg, flags flag){
#if EXPEDITUS_LOG_ENABLED == 1
	this->_header(flag);
	this->_file << msg;
#endif
}

Log& Log::operator<<(std::string msg){
	this->log(msg);
	return *this;
}

Log& Log::operator<<(char msg){
	this->log(msg);
	return *this;
}

Log& Log::operator<<(int msg){
	this->log(msg);
	return *this;
}