/*
* State.cpp
* Project: Expeditus
* Author: Michele Marolla (mic.marolla@gmail.com)
*/

#include "State.h"
#include "StateMachine.h"
#include <SFML/Graphics.hpp>

State::State(StateMachine *machine, sf::RenderWindow *window){
	this->_machine = machine;
	this->_window = window;
}