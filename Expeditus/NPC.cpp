/*
* NPC.cpp
* Project: Expeditus
* Author: Michele Marolla (mic.marolla@gmail.com)
*/

#include "NPC.h"
#include "Character.h"
#include "Consts.h"

NPC::NPC(int uid) : Character(uid){
	this->_havePattern = this->_patternPaused = false;
	this->_index = 0;
}

void NPC::startPattern(bool repeated){
	this->_pattern.clear();
	this->_havePattern = false;
	this->_repeated = repeated;
}

void NPC::addPatternVertex(int x, int y){
	this->_pattern.push_back(sf::Vector2i(x, y));
}

void NPC::addPatternVertex(sf::Vector2i vertex){
	this->_pattern.push_back(vertex);
}

void NPC::endPattern(){
	this->_havePattern = true;
}

sf::Vector2i NPC::getPatternVertex(int index) const{
	sf::Vector2i vertex(-1, -1);
	if (index >= 0 && index < this->_pattern.size())
		vertex = this->_pattern[index];
	return vertex;
}

void NPC::cancelPattern(){
	this->_index = 0;
	this->_havePattern = this->_patternPaused = false;
	this->_pattern.clear();
}

void NPC::_followPattern(float s){
	if (this->_index >= this->_pattern.size() || this->_patternPaused)
		return;

	sf::Vector2f curr = this->getPosition();
	sf::Vector2i next = this->_pattern[this->_index] * EXPEDITUS_MAP_TILE_SIZE;

	float offset = this->getPixelVelocity() * s;
	if (next.x != curr.x){		 // Movement on x-axis
		if (next.x > curr.x){
			if (curr.x + offset > next.x)
				this->move(next.x - curr.x, 0);
			else
				this->move(offset, 0);
		}else{
			if (curr.x - offset < next.x)
				this->move(next.x - curr.x, 0);
			else
				this->move(-offset, 0);
		}
	}else if (next.y != curr.y){ // Movement on y-axis
		if (next.y > curr.y){
			if (curr.y + offset > next.y)
				this->move(0, next.y - curr.y);
			else
				this->move(0, offset);
		}
		else{
			if (curr.y - offset < next.y)
				this->move(0, next.y - curr.y);
			else
				this->move(0, -offset);
		}
	}
	if (curr.x == float(next.x) && curr.y == float(next.y))
		this->_index++;
	if (this->_index >= this->_pattern.size() && this->_repeated)
		this->_index = 0;
}

void NPC::pausePattern(){
	this->_patternPaused = true;
}

void NPC::resumePattern(){
	this->_patternPaused = false;
}

void NPC::update(float s){
	Character::update(s);
	if (this->_havePattern)
		this->_followPattern(s);
}