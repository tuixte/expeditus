/*
* CardInterface.cpp
* Project: Expeditus
* Author: Michele Marolla (mic.marolla@gmail.com)
*/

#include "CardInterface.h"
#include <cstdlib>
#include "Card.h"
#include "Consts.h"
#include "Log.h"

void CardInterface::open(){
	Log::get() << "Opening " << EXPEDITUS_FILE_CARDLIST << " file...";
	this->_file.open(EXPEDITUS_FILE_CARDLIST, std::ios::in | std::ios::binary);
	if (this->_file.fail()){
		Log::get().flag(Log::flags::critic);
		Log::get().newline();  Log::get() << "Error on opening " << EXPEDITUS_FILE_CARDLIST << "file. Exit."; Log::get().newline();
		exit(1);
	}
	Log::get() << "done."; Log::get().newline();
}

void CardInterface::close(){
	this->_file.close();
}

Card CardInterface::get(int uid){
	Card card;

	// Check input
	if (uid <= 0){
		Log::get().flag(Log::flags::error);
		Log::get() << "Requested an invalid position in binary file (uid < 0)"; Log::get().newline();
		Log::get().flag(Log::flags::text);
		return card;
	}

	// Check if file is opened
	bool wasOpened = this->_file.is_open();
	if (!wasOpened)
		this->open();

	// Read file
	char name[EXPEDITUS_CARD_NAME_SIZE], type[EXPEDITUS_CARD_TYPE_SIZE], desc[EXPEDITUS_CARD_DESC_SIZE];
	this->_file.seekg((uid - 1)*EXPEDITUS_CARD_SIZE);
	this->_file.read((char*)&card.uid, EXPEDITUS_UID_SIZE);
	this->_file.read(name, EXPEDITUS_CARD_NAME_SIZE);
	card.name.assign(name);
	this->_file.read(type, EXPEDITUS_CARD_TYPE_SIZE);
	card.type.assign(type);
	this->_file.read((char*)&card.limit, sizeof(int));
	this->_file.read(desc, EXPEDITUS_CARD_DESC_SIZE);
	card.desc.assign(desc);

	if (!wasOpened)
		this->close();

	return card;
}