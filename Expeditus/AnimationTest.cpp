/*
* AnimationTest.cpp
* Project: Expeditus
* Author: Michele Marolla (mic.marolla@gmail.com)
*/

#include "AnimationTest.h"
#include <SFML\Graphics.hpp>

AnimationTest::AnimationTest(sf::RenderWindow *window) : Animated(16){
	this->_window = window;
	this->_texture.loadFromFile("res/CircleSpriteSheet.png");
	for (int i = 0; i < 16; i++){
		this->frameRects[i].left = 80 * (i % 4);
		this->frameRects[i].top = 80 * (i / 4);
		this->frameRects[i].width = 80;
		this->frameRects[i].height = 80;
	}
	this->frame = 0;
	this->_sprite.setTexture(this->_texture);
	this->_sprite.setPosition(200, 200);
	this->animClock.restart();
}

void AnimationTest::draw(){
	this->_window->draw(this->_sprite);

	int elapsed = this->animClock.getElapsedTime().asMilliseconds();
	if (elapsed < 60)
		return;
	
	this->frame += elapsed / 60;
	while (this->frame >= 16)
		this->frame -= 16;

	this->_sprite.setTextureRect(this->frameRects[this->frame]);
	this->animClock.restart();
}