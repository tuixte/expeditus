/*
* Game.cpp
* Project: Expeditus
* Author: Michele Marolla (mic.marolla@gmail.com)
*/

#include "Game.h"
#include <SFML\Window.hpp>
#include "Consts.h"

Game::Game() : _machine(&this->_window){}

void Game::run(){
	this->_window.create(sf::VideoMode(800, 600), "Expeditus", sf::Style::Close);
	this->_window.display();
	this->_machine.changeState(StateMachine::states::introduction);
	while (this->_window.isOpen()){
		this->_fpsClock.restart();
		this->_machine.currState->events();
		this->_machine.currState->update();
		this->_machine.currState->draw();
#if EXPEDITUS_FPS_CAPPING_ENABLED == 1
		if (this->_fpsClock.getElapsedTime().asMilliseconds() < 1000 / EXPEDITUS_FPS)
			sf::sleep(sf::milliseconds((1000 / EXPEDITUS_FPS) - this->_fpsClock.getElapsedTime().asMilliseconds()));
#endif
	}
}