/*
 * NPC.h
 * Project: Expeditus
 * Author: Michele Marolla (mic.marolla@gmail.com)
 */

#ifndef _NPC_H_
#define _NPC_H_

#include <vector>
#include <SFML\Graphics.hpp>
#include "Character.h"

/*! \brief Basic Non-Playing Character.
 *
 *	It is able to follow a pre-determined movement pattern. See startPattern() function.
 */
class NPC : public Character{
	
private:
	bool _havePattern;					//!< True if movement pattern is set, false else.
	std::vector<sf::Vector2i> _pattern; //!< Movement pattern
	bool _patternPaused;				//!< True if pattern is paused, false if it's going on (of it this NPC does not have one).
	int _index;							//!< Index of the current pattern step
	bool _repeated;						//!< True if pattern is continuously repeated; false else.

	/*! \brief Make the NPC move towards the next pattern's vertex, if set.
	*	\param[in] s Seconds elapsed since the last drawing cycle.
	*/
	void _followPattern(float s);


public:
	/*! \brief Constructor.
	 *  \param[in] uid NPC's uid.
	 */
	NPC(int uid);

	/*! Check if movement pattern is set.
	 *	\return True if movement pattern is set, false else.
	 */
	inline bool havePattern() const{ return this->_havePattern; }

	/*! \brief Start setting up a movement pattern.
	 *
	 *	To set up a movement pattern, call this function first.
	 *	Then, call addPatternVertex() function to add new vertexes into the pattern.
	 *	When you're done, call endPattern().
	 *	\param[in] repeated True if pattern is continuously repeated, false else.
	 *	\sa addPatternVertex(int, int) addPatternVertex(sf::Vector2i) endPattern()
	 */
	void startPattern(bool repeated);

	/*! \brief Add a new vertex into movement pattern.
	 *
	 *	To set up a movement pattern, call this function first.
	 *	Then, call addPatternVertex() function to add new vertexes into the pattern.
	 *	When you're done, call endPattern().
	 *	\param[in] x Destination tile's x coordinate
	 *	\param[in] y Destination tile's y coordinate
	 *	\sa startPattern() addPatternVertex(sf::Vector2i) endPattern()
	 */
	void addPatternVertex(int x, int y);

	/*! \brief Add a new vertex into movement pattern.
	*
	*	To set up a movement pattern, call this function first.
	*	Then, call addPatternVertex() function to add new vertexes into the pattern.
	*	When you're done, call endPattern().
	*	\param[in] vertex Destination tile's coordinates
	*	\sa startPattern() addPatternVertex(int, int) endPattern()
	*/
	void addPatternVertex(sf::Vector2i vertex);

	/*! \brief End movement pattern's setup.
	 *
	 *	To set up a movement pattern, call this function first.
	 *	Then, call addPatternVertex() function to add new vertexes into the pattern.
	 *	When you're done, call endPattern().
	 *	\sa startPattern() addPatternVertex(int, int) addPatternVertex(sf::Vector2i)
	 */
	void endPattern();

	/*! \brief Get pattern's vertex at given index.
	 *	\param[in] index Index of the wanted vertex.
	 *	\return Coordinates of the vertex.
	 */
	sf::Vector2i getPatternVertex(int index) const;

	/*! \brief Get current movement pattern's index.
	 *	\return Movement pattern's index currently in use.
	 */
	inline int getCurrentPatternIndex() const{ return this->_index; }

	/*!	\brief Get current pattern's vertex.
	 *	\return Current pattern's vertex.
	 */
	inline sf::Vector2i getCurrentPatternVertex() const{ return this->_pattern[this->_index]; }

	/*! \brief Delete current pattern.
	 */
	void cancelPattern();

	/*! \brief Pause NPC's movement pattern.
	 *	\sa resumePattern() isPatternPaused()
	 */
	void pausePattern();

	/*! \brief Resume NPC's movement pattern.
	 *	\sa pausePattern() isPatternPaused()
	 */
	void resumePattern();

	/*! \brief Check if NPC's movement pattern is paused.
	 *	\return True if pattern is paused, false if else (or if this NPC does not have a pattern at all).
	 *	\sa pausePattern() resumePattern()
	 */
	inline bool isPatternPaused() const{ return this->_patternPaused; }

	/*! \brief Update NPC's position.
	*
	*	It also makes this NPC follow its pattern, if it has one.
	*	\param[in] s Seconds passed since the last drawing cycle. This argument is automatically given by TiledMap if you
	*	use TiledMap::update() function (recommended).
	*	\sa Character::update() TiledMap::update() startPattern()
	*/
	void update(float s);

};

#endif