/* 
 * State.h
 * Project: Expeditus
 * Author: Michele Marolla (mic.marolla@gmail.com)
 */

#ifndef _STATE_H_
#define _STATE_H_

//#include "StateMachine.h"
#include <SFML/Graphics.hpp>

class StateMachine;

/*! \brief A game state. Pure virtual class.
 *
 *	Every game state have to inherit this class.
 *  \sa States.h IntroductionState
 */
class State{

protected:
	StateMachine *_machine;		//!< Pointer to game's state machine
	sf::RenderWindow *_window;  //!< Game's render window.
	bool _paused;				//!< True if state is paused, false else.

public:
	/*! \brief Constructor
	 *
	 *	\param[in] machine Pointer to game's state machine.
	 *	\param[in] window Game's render window.
	 */
	State(StateMachine *machine, sf::RenderWindow *window);

	/*! \brief Initialize game resources */
	virtual void init() = 0;

	/*! \brief Handle events */
	virtual void events() = 0;

	/*! \brief Update state */
	virtual void update() = 0;

	/*! \brief Draw state */
	virtual void draw() = 0;

	/*! \brief Release resources */
	virtual void cleanup() = 0;

	/*! \brief Pause the state
	 *	\sa resume() isPaused() */
	inline void pause(){ this->_paused = true; }

	/*! \brief Resume the state
	 *	\sa pause() isPaused() */
	inline void resume(){ this->_paused = false; }

	/*! \brief Check if the state is paused
	 *	\return True if the state is paused, false else.
	 *	\sa pause() resume()
	 */
	inline bool isPaused(){ return this->_paused; }

};

#endif