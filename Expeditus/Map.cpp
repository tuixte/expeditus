/*
* Map.cpp
* Project: Expeditus
* Author: Michele Marolla (mic.marolla@gmail.com)
*/

#include "Map.h"
#include <string>
#include <SFML\Graphics.hpp>
#include "Character.h"
#include "Log.h"

Map::Map(){
	this->_texture = 0;
	this->_ready = false;
}

Map::Map(std::string filepath) : Map(){
	this->loadTexture(filepath);
}

void Map::loadTexture(std::string filepath){
	this->_texture = new sf::Texture();
	Log::get() << "Loading texture from '" << filepath << "'...";
	if (!this->_texture->loadFromFile(filepath)){
		Log::get().newline(); Log::get().log("Unable to load texture", Log::flags::error); Log::get().newline();
		return;
	}
	Log::get() << "done."; Log::get().newline();
	this->_sprite.setTexture(*this->_texture);
	this->_sprite.setPosition(0, 0);
	this->_ready = true;
}

void Map::setPosition(int x, int y){
	this->_sprite.setPosition(x, y);
}

void Map::setTexture(sf::Texture *texture){
	this->_texture = texture;
	this->_sprite.setTexture(*this->_texture);
	this->_sprite.setPosition(0, 0);
	this->_ready = true;
}

void Map::cleanup(){
	if (this->_texture)
		delete this->_texture;
}

void Map::follow(Character *toFollow, sf::View *view){
	// Follow character
	if (!toFollow) return;
	sf::Vector2f center = view->getCenter();
	sf::Vector2f charPos = toFollow->getPosition();
	// x
	if (charPos.x < view->getSize().x / 2)
		center.x = view->getSize().x / 2;
	else if (charPos.x > this->_texture->getSize().x - view->getSize().x / 2)
		center.x = this->_texture->getSize().x - view->getSize().x / 2;
	else
		center.x = charPos.x;
	// y
	if (charPos.y < view->getSize().y / 2)
		center.y = view->getSize().y / 2;
	else if (charPos.y > this->_texture->getSize().y - view->getSize().y / 2)
		center.y = this->_texture->getSize().y - view->getSize().y / 2;
	else
		center.y = charPos.y;
	view->setCenter(center);
}

void Map::draw(sf::RenderTarget &target, sf::RenderStates states) const{
	if (!this->_ready) return;
	target.draw(this->_sprite);
}