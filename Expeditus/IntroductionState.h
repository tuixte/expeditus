/*
 * IntroductionState.h
 * Project: Expeditus
 * Author: Michele Marolla (mic.marolla@gmail.com)
 */

#ifndef _INTRODUCTION_STATE_H_
#define _INTRODUCTION_STATE_H_

#include <SFML\Graphics.hpp>
#include "AnimationTest.h"
#include "DialogueManager.h"
#include "State.h"
#include "StateMachine.h"

/*! \brief Introduction state.
 *	\sa State States.h
 */
class IntroductionState : public State{

private:
	DialogueManager _manager;	//!< Dialogue manager
	AnimationTest animTest;		//!< Animation test

public:
	/*! \brief Constructor
	 *	\param[in] machine Pointer to game's state machine.
	 *	\param[in] window Pointer to game's render window.
	 */
	IntroductionState(StateMachine *machine, sf::RenderWindow *window);

	/*! \brief Initialize resources */
	void init();

	/*! \brief Handle events */
	void events();

	/*! \brief Update state */
	void update();

	/*! \brief Draw state */
	void draw();

	/*! \brief Release resources */
	void cleanup();

};

#endif