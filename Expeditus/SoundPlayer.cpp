/*
 * SoundPlayer.cpp
 * Project: Expeditus
 * Author: Michele Marolla (mic.marolla@gmail.com)
 */

#include "SoundPlayer.h"
#include <string>
#include <SFML\Audio.hpp>
#include "Log.h"

SoundPlayer::SoundPlayer(){
	this->_ready = false;
}

void SoundPlayer::load(std::string path){
	Log::get() << "Loading sound from file '" << path << "'...";
	if (!this->_buffer.loadFromFile(path)){
		Log::get().flag(Log::flags::error);
		Log::get().newline();  Log::get() << "Cannot open file '" << path << "'"; Log::get().newline();
		Log::get().flag(Log::flags::text);
		return;
	}
	Log::get() << "done."; Log::get().newline();
	this->_ready = true;
	this->_sound.setBuffer(this->_buffer);
}

void SoundPlayer::play(){
	if (!this->_ready){
		Log::get().log("Tried to play a sound without loading any file", Log::flags::alert);
		Log::get().newline();
		return;
	}
	this->_sound.play();
}

void SoundPlayer::play(std::string path){
	Log::get() << "Loading sound from file '" << path << "'...";
	if (!this->_buffer.loadFromFile(path)){
		Log::get().flag(Log::flags::error);
		Log::get().newline();  Log::get() << "Cannot open file '" << path << "'"; Log::get().newline();
		Log::get().flag(Log::flags::text);
		return;
	}
	Log::get() << "done."; Log::get().newline();
	this->_sound.setBuffer(this->_buffer);
	this->_ready = true;
	this->_sound.play();
}