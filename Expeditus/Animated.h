/*
 * Animated.h
 * Project: Expeditus
 * Author: Michele Marolla (mic.marolla@gmail.com)
 */

#ifndef _ANIMATED_H_
#define _ANIMATED_H_

#include <SFML\System.hpp>
#include <SFML\Graphics.hpp>

//!	\brief Basic interface for animated entity.
class Animated{

protected:
	sf::Clock animClock;		//!< Clock used to perform animation.
	int frame;					//!< Current frame.
	sf::IntRect *frameRects;	//!< Texture's portions for every frame.

public:
	/*! \brief Constructor
	 *	\param frames Number of frames
	 */
	Animated(int frames);

	//! Destructor
	~Animated();

};

#endif