/*
 * Dialogue.h
 * Project: Expeditus
 * Author: Michele Marolla (mic.marolla@gmail.com)
 */

#ifndef _DIALOGUE_H_
#define _DIALOGUE_H_

#include <string>

/*! \brief Basic structure of a dialogue.
 *
 *	It contains UID (unique id), name of the character that speaks,
 *	dialogue's text and other information.
 *	\sa DialogueManager
 */
struct Dialogue{
	//! Dialogue Unique Identifier number
	int uid = 0;

	//! Name of the character that speaks
	std::string character;

	//! Text of the dialogue
	std::string text;

	/*! \brief UID of the next dialogue to show 
	 *	
	 *	Dialogues can be linked together with NEXT clause. For example, if dialogue with UID 1
	 *	has clause NEXT: 2, then after showing dialogue 1, dialogue 2 will be shown.
	 *	If it's equal to 0, then this dialogues is not linked to other ones.
	 *	\sa DialogueManager::start(int) DialogueManager::alone(int)
	 */
	int next_uid = 0;

	//! True if this dialogue is a question that need an answer yes/no; false else.
	bool answer = false;

	//! True if this dialogue is linked to a script, false if not
	bool script = false;
};

#endif