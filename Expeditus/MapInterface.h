/*
 * MapInterface.h
 * Project: Expeditus
 * Author: Michele Marolla (mic.marolla@gmail.com)
 */

#ifndef _MAP_INTERFACE_H_
#define _MAP_INTERFACE_H_

#include <fstream>
#include <string>
#include "TiledMap.h"

/*! \brief Interface to read maps from binary files.
 *
 *	Map syntax:
 *	Map_width Map_height (number of tiles)
 *	Player_pos_x Player_pos_y (in tiles)
 *	For each tile:
 *		Movement (0 or 1)
 *		Event (0 or 1)
 *	\sa TiledMap
 */
class MapInterface{

private:
	std::fstream _file; //< Map binary file

public:
	/*! \brief Open map file. in binary mode
	 *	\param[in] path Map file path
	 *	\sa isOpen() close()
	 */
	void open(std::string path);

	/*! \brief Check if any map file has been opened.
	 *	\return True if map file has ben opened, false else.
	 *	\sa open() close()
	 */
	inline bool isOpen() const { return this->_file.is_open(); }

	/*! \brief Close map file.
	 *	\sa open() isOpen()
	 */
	void close();

	/*! \brief Read map file and store information into a map object.
	 *	\param[out] map Pointer to map that will be store read information.
	 */
	void get(TiledMap *map);

};

#endif