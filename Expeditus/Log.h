/*
 * Log.h
 * Project: Expeditus
 * Author: Michele Marolla (mic.marolla@gmail.com)
 */

#ifndef _LOG_H_
#define _LOG_H_

#include <ctime>
#include <fstream>
#include <string>

/*!
 * \brief Simple logger that writes on a text file.
 *
 * It's a singleton class, use Log::get() to retrieve a valid istance.
 * Can print date and time; supports four different types of messages: 'text', 'alert', 'error', 'critic'.
 * Default one is text.
 * \sa Log::get() Log::flags
 */
class Log{

public:
	//! Logging flags
	enum flags{
		text,
		alert,
		error,
		critic
	};

private:
	std::fstream _file;	//!< File in which logger will write
	flags _flag;		//!< Current logging flag (default: logs::text)
	tm _timeinfo;		//!< Information about date and time

	Log(); //!< Constructor
	Log(Log const&); //!< Copy constructor, not implemented
	void operator=(Log const&); //!< Operator= not implemented
	~Log(); //!< Destructor

	/*! \brief Write current logging file on class.
	 *
	 *	This function is used internally.
	 *	Use log() or operator<< to write something on log file.
	 *	\param[in] flag Flag in use
	 *  \sa flags log(std::string, flags) log(char, flags) log(int, flags) operator<<(std::string) operator<<(char) operator<<(int)
	 */
	void _header(flags flag);

public:
	//! Returns Log istance.
	static Log& get(){
		static Log instance;
		return instance;
	}

	/*! \brief Set flag value (text, alert, error or critic)
	 *	\param[in] flag New flag
	 *	\sa flags flag()
	 */
	void flag(flags flag);

	/*! \brief Return current flag value (text, alert, error or critic).
	 *
	 *	Default flag value is logs::text.
	 *	\sa flags flag(flags)
	 */
	inline flags flag() const { return this->_flag; }

	/*! \brief Go to a new line */
	void newline();

	/*! \brief Print date and time */
	void printTime();

	/*! \brief Log something.
	 *	\param[in] msg String to log.
	 *	\param[in] flag Flag to use (default flags::text).
	 *	\sa log(char, flags) log(int, flags) operator<<(std::string) operator<<(char) operator<<(int)
	 */
	void log(std::string msg, flags flag=flags::text);

	/*! \brief Log something.
	*	\param[in] msg Single char to log.
	*	\param[in] flag Flag to use (default flags::text).
	*	\sa log(std::string, flags) log(int, flags) operator<<(std::string) operator<<(char) operator<<(int)
	*/
	void log(char msg, flags flag = flags::text);

	/*! \brief Log something.
	*	\param msg Integer number to log.
	*	\param flag Flag to use (default flags::text)
	*	\sa log(std::string, flags) log(char, flags) operator<<(std::string) operator<<(char) operator<<(int)
	*/
	void log(int msg, flags flag = flags::text);

	/*! \brief Log something.
	*
	*	This function class the correct log() function using current Log::_flag flag.
	*	Call Log::flag(flags) and Log::flag() to set/get it.
	*	\param[in] msg String to log.
	*	\return Reference the Log instance.
	*	\sa flag(flags) flag() log(std::string, flags) log(char, flags) log(int, flags) operator<<(char) operator<<(int)
	*/
	Log& operator<<(std::string msg);

	/*! \brief Log something.
	*
	*	This function class the correct log() function using current Log::_flag flag.
	*	Call Log::flag(flags) and Log::flag() to set/get it.
	*	\param[in] msg Single char to log.
	*	\return Reference the Log instance.
	*	\sa flag(flags) flag() log(std::string, flags) log(char, flags) log(int, flags) operator<<(std::string) operator<<(int)
	*/
	Log& operator<<(char msg);

	/*! \brief Log something.
	*
	*	This function class the correct log() function using current Log::_flag flag.
	*	Call Log::flag(flags) and Log::flag() to set/get it.
	*	\param[in] msg Integer number to log.
	*	\return Reference the Log instance.
	*	\sa flag(flags) flag() log(std::string, flags) log(char, flags) log(int, flags) operator<<(std::string) operator<<(char)
	*/
	Log& operator<<(int msg);

};

#endif