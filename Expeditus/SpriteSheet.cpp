/*
* SpriteSheet.cpp
* Project: Expeditus
* Author: Michele Marolla (mic.marolla@gmail.com)
*/

#include "SpriteSheet.h"
#include <string>
#include <SFML\Graphics.hpp>
#include "Log.h"

SpriteSheet::SpriteSheet(){
	this->_ready = false;
}

void SpriteSheet::load(std::string texture_path){
	this->_ready = true;
	Log::get() << "Loading texture from '" << texture_path << "'...";
	if (!this->_texture.loadFromFile(texture_path)){
		Log::get().newline(); Log::get().log("Unable to load texture", Log::flags::error); Log::get().newline();
	}
	Log::get() << "done."; Log::get().newline();
}

void SpriteSheet::get(sf::Sprite *sprite, int x, int y, int w, int h){
	if (!this->_ready){
		Log::get().log("Tried to get sprite from spritesheet without loading any texture", Log::flags::error);
		return;
	}
	sprite->setTexture(this->_texture);
	sprite->setTextureRect(sf::IntRect(x, y, w, h));
}