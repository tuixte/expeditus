/*
 * Consts.h
 * Project: Expeditus
 * Author: Michele Marolla (mic.marolla@gmail.com)
 */

#ifndef _CONSTS_H_
#define _CONSTS_H_

/*!
 * \file Consts.h
 * \brief Various costants used in game.
 */


/*! \defgroup FILEPATHS Constants about file paths.
 *	@{ */
//! Log file path.
#define EXPEDITUS_FILE_LOG "log.txt"
//! Card list file path.
#define EXPEDITUS_FILE_CARDLIST "res/cards.bin"
//! Save file path.
#define EXPEDITUS_FILE_SAVING "res/save.bin"
/*! @} */


//! Size of UIDs. It's equal to size of an int.
#define EXPEDITUS_UID_SIZE sizeof(int)
//! Maximum number of chars for characters's name.
#define EXPEDITUS_CHARACTER_NAME_SIZE 20


/*! \defgroup CARDSIZE Constants about card and deck size.
 *	@{ */
//! Maximum number of chars for card's name.
#define EXPEDITUS_CARD_NAME_SIZE 20
//! Maximum number of chars for card's type.
#define EXPEDITUS_CARD_TYPE_SIZE 8
//! Maximum number of chars for card's description.
#define EXPEDITUS_CARD_DESC_SIZE 200
//! Total size of type Card.
#define EXPEDITUS_CARD_SIZE (EXPEDITUS_UID_SIZE + EXPEDITUS_CARD_NAME_SIZE + EXPEDITUS_CARD_TYPE_SIZE + EXPEDITUS_CARD_DESC_SIZE)
//! Maximum number of cards in deck.
#define EXPEDITUS_DECK_MAX_SIZE  40
//! Total number of cards in game
#define EXPEDITUS_CARDS_NUMBER 2
/*! @} */


/*! \defgroup DIALOGUES Constants about dialogues's size, position and delay.
 *	@{ */
//! Maximum number of chars in dialogue's text.
#define EXPEDITUS_DIALOGUE_TEXT_MAX_SIZE 500
//! Delay (in milliseconds) in printing dialogue's text.
#define EXPEDITUS_DIALOGUE_DELAY		50 // milliseconds
//! X coordinate of dialogue's text.
#define EXPEDITUS_DIALOGUE_POS_X		10
//! Y coordinate of dialogue's text.
#define EXPEDITUS_DIALOGUE_POS_Y		500
//! Limit x coordinate of dialogue's text.
#define EXPEDITUS_DIALOGUE_POS_LIMIT_X	790
//! Limit y coordinate of dialogues' text.
#define EXPEDITUS_DIALOGUE_POS_LIMIT_Y	590
//! X coordinate of yes/no answers position.
#define EXPEDITUS_DIALOGUE_A_POS_X		745
//! Y coordinate of yes answer position.
#define EXPEDITUS_DIALOGUE_A_POS_Y1		435
//! Y coordinate of no answer position.
#define EXPEDITUS_DIALOGUE_A_POS_Y2		457
//! Delay (in milliseconds) in showing dialogue cursor.
#define EXPEDITUS_DIALOGUE_CURSOR_DELAY 500 // milliseconds
//! X coordinate of dialogue cursor.
#define EXPEDITUS_DIALOGUE_CURSOR_X		775
//! Y coordinate of dialogue cursor.
#define EXPEDITUS_DIALOGUE_CURSOR_Y		580
//! X coordinate of dialogue answering cursor.
#define EXPEDITUS_DIALOGUE_A_CURSOR_X	733
//! Y coordinate of dialogue answering cursor (select YES).
#define EXPEDITUS_DIALOGUE_A_CURSOR_Y1	442
//! Y coordinate of dialogue answering cursor (select NO).
#define EXPEDITUS_DIALOGUE_A_CURSOR_Y2	464
//! Left coordinate of dialogue's rectangle.
#define EXPEDITUS_DIALOGUE_RECT_LEFT		1
//! Right coordinate of dialogue's rectangle.
#define EXPEDITUS_DIALOGUE_RECT_RIGHT		784
//! Top coordinate of dialogue's rectangle.
#define EXPEDITUS_DIALOGUE_RECT_TOP			490
//! Bottom coordinate of dialogue's rectangle.
#define EXPEDITUS_DIALOGUE_RECT_BOTTOM		583
//! Left coordinate of yes/no answers's rectangle.
#define EXPEDITUS_DIALOGUE_A_RECT_LEFT		721
//! Right coordinate of yes/no answers's rectangle.
#define EXPEDITUS_DIALOGUE_A_RECT_RIGHT		784
//! Top coordinate of yes/no answers's rectangle.
#define EXPEDITUS_DIALOGUE_A_RECT_TOP		430
//! Bottom coordinate of yes/no answers's rectangle.
#define EXPEDITUS_DIALOGUE_A_RECT_BOTTOM	474
/*! @} */


/*! \defgroup MAPS Constants about map's size.
 * @{ */
//! Max map's width (number of tiles).
#define EXPEDITUS_MAP_MAX_WIDTH		20
//! Max map's height (number of tiles).
#define EXPEDITUS_MAP_MAX_HEIGHT	20
//! Map tiles's side (every tile is a square).
#define EXPEDITUS_MAP_TILE_SIZE		50
/*! @} */

/*!
 * \brief Enable logging.
 * 
 * Set this to 1 to enable logging, 0 else.
 * \sa EXPEDITUS_FILE_LOG Log
 */
#define EXPEDITUS_LOG_ENABLED 1


/*! \defgroup FPS Constants about fps value.
* @{ */
/*!
* \brief Enable FPS capping.
*
* Set this to 1 to enable FPS capping, 0 else.
* \sa EXPEDITUS_FPS Game
*/
#define EXPEDITUS_FPS_CAPPING_ENABLED 0
/*!
 * \brief FPS limit value
 *
 * It is effective only if EXPEDITUS_FPS_CAPPING_ENABLED is set to 1.
 * \sa EXPEDITUS_FPS_CAPPING_ENABLED Game
 */
#define EXPEDITUS_FPS 60
/*! @} */

#endif



// Doxygen Main Page
/*! \mainpage Expeditus Main Page
Expeditus is a small 2D adventure videogame, with a bird&apos;-eye view style like Pok&eacute;mon.
The title is latin for quick, armed, ready.

Fundamental is the trading-card system: the player will be able to collect and use cards in order to perform every action.

For futher information, please visit:
	- my blog <http://tuixte.wordpress.com>
	- official repository <http://bitbucket.org/tuixte/expeditus>
*/