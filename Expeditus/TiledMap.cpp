/*
* TiledMap.cpp
* Project: Expeditus
* Author: Michele Marolla (mic.marolla@gmail.com)
*/

#include "TiledMap.h"
#include <SFML\Graphics.hpp>
#include "Character.h"
#include "MapInterface.h"
#include "NPC.h"

TiledMap::TiledMap() : Map(){
	this->_tWidth = this->_tHeight = 0;
	this->_playerX = this->_playerY = 0;
	this->_player = 0;
}

TiledMap::TiledMap(std::string filepath) : Map(filepath){
	this->_tWidth = this->_tHeight = 0;
	this->_playerX = this->_playerY = 0;
	this->_player = 0;
}

sf::Vector2i TiledMap::getTileSize() const{
	return sf::Vector2i(this->_tWidth, this->_tHeight);
}

Tile TiledMap::getTile(int x, int y) const{
	Tile tile;
	if (x >= 0 && x < this->_tWidth && y >= 0 && y < this->_tHeight)
		tile = this->data[x][y];
	return tile;
}

void TiledMap::addCharacter(Character *character){
	character->map = this;
}

void TiledMap::addCharacter(NPC *character){
	character->map = this;
	this->_npcs.push_back(character);
}

void TiledMap::addPlayer(Player *player){
	player->map = this;
	this->_player = player;
}

void TiledMap::loadMap(std::string filepath){
	MapInterface interface;
	interface.open(filepath);
	interface.get(this);
	interface.close();
}

bool TiledMap::move(Character *character, float offX, float offY){
	if (offX == 0 && offY == 0)
		return true;

	sf::Vector2u size = character->getSize();

	sf::FloatRect charRect = character->getBounds();
	charRect.left += offX;
	charRect.top += offY;

	// Retrieve destination tile
	int tx = -1; // if offX == 0
	if (offX < 0)
		tx = charRect.left / EXPEDITUS_MAP_TILE_SIZE;
	else if(offX > 0)
		tx = (charRect.left + size.x) / EXPEDITUS_MAP_TILE_SIZE;
	
	int ty = -1; // if offY == 0
	if (offY < 0)
		ty = charRect.top / EXPEDITUS_MAP_TILE_SIZE;
	else if(offY > 0)
		ty = (charRect.top + size.y) / EXPEDITUS_MAP_TILE_SIZE;
	
	// Check tile's movement permissions
	if (tx == -1){ // surely, ty != -1
		if (!this->data[int(charRect.left / EXPEDITUS_MAP_TILE_SIZE)][ty].movement ||
			!this->data[int((charRect.left + size.x) / EXPEDITUS_MAP_TILE_SIZE)][ty].movement)
			return false;
	}else if (ty == -1){ // surely tx != -1
		if (!this->data[tx][int(charRect.top / EXPEDITUS_MAP_TILE_SIZE)].movement ||
			!this->data[tx][int((charRect.top + size.y) / EXPEDITUS_MAP_TILE_SIZE)].movement)
			return false;
	}else{
		if (!this->data[tx][ty].movement)
			return false;
	}

	// Check collisions
	if (character->uid != this->_player->uid)
		if (charRect.intersects(this->_player->getBounds()))
			return false;

	for (std::vector<NPC*>::iterator it = this->_npcs.begin(); it != this->_npcs.end(); it++){
		if (character->uid == (*it)->uid)
			continue;
		if (charRect.intersects((*it)->getBounds()))
			return false;
	}
	
	return true;
}

void TiledMap::update(){
	for (std::vector<NPC*>::iterator it = this->_npcs.begin(); it != this->_npcs.end(); it++)
		(*it)->update(this->_clock.getElapsedTime().asSeconds());
	this->_player->update(this->_clock.getElapsedTime().asSeconds());
}

void TiledMap::restartClock(){
	this->_clock.restart();
}