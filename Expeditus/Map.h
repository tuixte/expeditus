/*
 * Map.h
 * Project: Expeditus
 * Author: Michele Marolla (mic.marolla@gmail.com)
 */

#ifndef _MAP_H_
#define _MAP_H_

#include <string>
#include <SFML\Graphics.hpp>

class Character;

/*! \brief Generic game's map.
 *
 *	This class can be used for levels's background or minimap, for example.
 *	If you need to handle level's tiles (for movement permissions, for example) use TiledMap.
 *	\sa TiledMap
 */
class Map : public sf::Drawable{

private:
	sf::Texture *_texture;	//! Map's texture.
	sf::Sprite _sprite;		//! Sprite used to draw map's texture.
	bool _ready;			//! True if any texture has been loaded, false else.

public:
	/*! \brief Constructor.
	 *	\sa Map(std::string)
	 */
	Map();

	/*! \brief Constructor.
	 *
	 *	It is equivalent than calling Map() default constructor and load() function.
	 *	\param[in] filepath Texture's file path.
	 *	\sa Map() load()
	 */
	Map(std::string filepath);

	/*! \brief Load texture from file.
	 *
	 *	It is equivalent than calling Map(std::string) constructor.
	 *	\param[in] filepath Texture's file path.
	 *	\sa Map(std::string) setTexture() cleanup()
	 */
	void loadTexture(std::string filepath);

	/*! \brief Set texture's rendering position.
	 *
	 *	Position is relative to game's world, not window.
	 *	\param[in] x Texture render's x coordinate
	 *	\param[in] y Texture render's y coordinate
	 */
	void setPosition(int x, int y);

	/*! \brief Set map's texture.
	 *
	 *	Use this function when you're sure that texture will not be destructed
	 *	while this Map object is still using it.
	 *	For example, you can use this function to have multiple views of the
	 *	same background, loading its texture only once.
	 *	\param[in] texture Pointer to map's texture.
	 *	\sa setTexture() load()
	 */
	void setTexture(sf::Texture *texture);

	/*! \brief Get a pointer to map's texture.
	 *
	 *	You can use this function together with setTexture() to share a single texture
	 *	with multiple maps.
	 *	\return Pointer to map's texture; null pointer if no texture has been loaded.
	 *	\sa setTexture()
	 */
	inline sf::Texture* const getTexture() const{ return this->_texture; }

	/*! \brief Delete map's texture, if loaded.
	 *
	 *	Pay attention: if you share a single texture with multiple maps, call this
	 *	function only in the one that loaded the texture.
	 *	If you loaded your texture manually, do not call this at all.
	 *	\sa load()
	 */
	void cleanup();

	/*!	\brief Check if map's texture has been loaded.
	 *	\return True if texture has been loaded, false else.
	 *	\sa load() setTexture()
	 */
	inline bool isReady() const{ return this->_ready; }

	/*! \brief Get texture's size.
	 *	\return Texture's width and height.
	 */
	sf::Vector2u getSize() const{ return this->_texture->getSize(); }

	/*!	\brief Let the view follow a character.
	 *
	 *	If possible, set view's center in the character's position.
	 *	The view will never go out of bounds: if the character is too close to map's bound
	 *	the view's center is not moved.
	 *	\param[in] toFollow Pointer to the character to follow.
	 *	\param[in] view Pointer to view
	 */
	void follow(Character *toFollow, sf::View *view);

	/*!	\brief Draw map's texture using the current view and viewport.
	 *	\param[in] target Render target
	 *	\param[in] states Render states
	 */
	void draw(sf::RenderTarget &target, sf::RenderStates states) const;

};

#endif