/*
 * CardInterface.h
 * Project: Expeditus
 * Author: Michele Marolla (mic.marolla@gmail.com)
 */

#ifndef _CARD_INTERFACE_H_
#define _CARD_INTERFACE_H_

#include <fstream>
#include "Card.h"

/*! \brief Interface to read card information from binary file.
 *
 *	Card syntax:
 *	UID Card_name Card_type Card_description
 *	\sa Card EXPEDITUS_FILE_CARDLIST
 */
class CardInterface{

private:
	std::fstream _file; //!< Cards list file

public:
	/*! \brief Open cards list file.
	 *	\sa isOpen() close() */
	void open();

	/*! \brief Check if cards list file has been opened.
	 *	\return True if cards list file has been opened, false else.
	 *	\sa open() close()
	 */
	inline bool isOpen() const { return this->_file.is_open(); }

	/*! \brief Open cards list file.
	*	\sa open() isOpen() */
	void close();

	/*! \brief Read card with given uid from file.
	 *
	 *	If file has not been opened, the function automatically open cards list
	 *	file, and close it when done.
	 *	If you need to read a lot of cards, it is more efficient to open and close
	 *	file manually.
	 *	\param[in] uid Identifier of the wanted card.
	 *	\return The wanted card; if nout found, empty card (uid: 0).
	 *	\sa open() isOpen() close()
	 */
	Card get(int uid);

};

#endif