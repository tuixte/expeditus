/*
* StateMachine.cpp
* Project: Expeditus
* Author: Michele Marolla (mic.marolla@gmail.com)
*/

#include "StateMachine.h"
#include "Log.h"
#include "State.h"
#include "States.h"
#include <SFML\Graphics.hpp>

StateMachine::StateMachine(sf::RenderWindow *window){
	this->currState = this->_previousState = 0;
	this->_window = window;
}

StateMachine::~StateMachine(){
	if (this->currState)
		this->currState->cleanup();
}

void StateMachine::changeState(states nextState){
	Log::get() << "Switching to state: ";
	if (this->currState)
		this->currState->cleanup();
	State *next = 0;
	switch (nextState){
	case states::previous:
		next = this->_previousState;
		Log::get() << "previous state";
		break;
	case states::introduction:
		next = new IntroductionState(this, this->_window);
		Log::get() << "introduction";
		break;
	case states::game:
		next = new GameState(this, this->_window);
		Log::get() << "game";
		break;
	// case states::menu
		// Log::get() << "menu";
	// ...
	default:
		break;
	}
	delete this->currState;
	this->_previousState = 0;
	this->currState = next;
	Log::get().newline();
	this->currState->init();
}

void StateMachine::setState(states nextState){
	Log::get() << "Switching to state: ";
	State *next = 0;
	switch (nextState){
	case states::previous:
		next = this->_previousState;
		Log::get() << "previous state";
		break;
	case states::introduction:
		next = new IntroductionState(this, this->_window);
		Log::get() << "introduction";
		break;
	case states::game:
		next = new GameState(this, this->_window);
		Log::get() << "game";
		break;
		// case states::menu
		// Log::get() << "menu";
		// ...
	default:
		break;
	}
	this->_previousState = this->currState;
	this->currState = next;
	Log::get().newline();
	this->currState->init();
}