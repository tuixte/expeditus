/*
* MapInterface.cpp
* Project: Expeditus
* Author: Michele Marolla (mic.marolla@gmail.com)
*/

#include "MapInterface.h"
#include <string>
#include "Consts.h"
#include "Log.h"
#include "TiledMap.h"

void MapInterface::open(std::string path){
	Log::get() << "Opening " << path << " file...";
	this->_file.open(path, std::ios::in | std::ios::binary);
	if (this->_file.fail()){
		Log::get().flag(Log::flags::critic);
		Log::get().newline();  Log::get() << "Error on opening " << path << "file. Exit."; Log::get().newline();
		exit(1);
	}
	Log::get() << "done."; Log::get().newline();
}

void MapInterface::close(){
	this->_file.close();
}

void MapInterface::get(TiledMap *map){
	/* Open file */
	Log::get() << "Reading map...";

	// Check if file is opened
	if (!this->_file.is_open()){
		Log::get().log("Tried to read a dialogue without opening any file.", Log::flags::error);
		return;
	}

	/* Read file */
	int posX, posY;
	this->_file.seekg(0);
	this->_file.read((char*)&map->_tWidth, sizeof(int));
	this->_file.read((char*)&map->_tHeight, sizeof(int));
	this->_file.read((char*)&posX, sizeof(int));
	map->_playerX = posX * EXPEDITUS_MAP_TILE_SIZE;
	this->_file.read((char*)&posY, sizeof(int));
	map->_playerY = posY * EXPEDITUS_MAP_TILE_SIZE;
	for (int i = 0; i < map->_tHeight; i++){
		for (int j = 0; j < map->_tWidth; j++){
			this->_file.read((char*)&(map->data[j][i].movement), sizeof(bool));
			this->_file.read((char*)&(map->data[j][i].event), sizeof(bool));
		}
	}

	Log::get() << "done."; Log::get().newline();
}