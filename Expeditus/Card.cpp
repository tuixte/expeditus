/*
* Card.cpp
* Project: Expeditus
* Author: Michele Marolla (mic.marolla@gmail.com)
*/

#include "Card.h"

Card::Card(){
	this->uid = 0;
}

void Card::clear(){
	this->uid = 0;
	this->limit = 0;
	this->name.clear();
	this->type.clear();
	this->desc.clear();
}