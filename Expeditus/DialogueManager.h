/*
 * DialogueManager.h
 * Project: Expeditus
 * Author: Michele Marolla (mic.marolla@gmail.com)
 */

#ifndef _DIALOGUE_MANAGER_H_
#define _DIALOGUE_MANAGER_H_

#include <string>
#include <SFML\Graphics.hpp>
#include "BitmapFont.h"
#include "Dialogue.h"
#include "DialogueInterface.h"
#include "SpriteSheet.h"

/*! \brief Simple manager that handles dialogues.
 *
 *	It can start a chain, that is starting from one message, and then pass to the
 *	one indicated by 'next_uid' value, until the chain finishes;
 *	or print just one message.
 *	\sa Dialogue
 */
class DialogueManager{

private:
	sf::RenderWindow *_window;				//!< Pointer to game's render window.
	BitmapFont _font;						//!< Dialogues's text font.
	DialogueInterface _dialogueInterface;	//!< Interface to read dialogues from binary files.
	SpriteSheet _sheet;						//!< Sprite sheet to handle dialogues's rectangle and cursors.
	bool _working;							//!< True if any dialogue is currently being printed, false else.
	bool _delaying;							//!< True if any dialogue is currently being printed with delay, false else.
	bool _series;							//!< True if a series of dialogues is currently being printed, false else.
	bool _divided;							//!< True if a single dialogue has been split in parts, false else.
	bool _done;								//!< True if a single divided dialogue has been completely printed, false else.
	Dialogue _dialogue;						//!< Current dialogue.
	int _limitStart;						//!< Index of the first character to print (used for divided dialogues).
	int _limitEnd;							//!< Index of the last character to print (used for divided dialogues).
	int _nextLimitStart;					//!< Index of the next first character to print (used for divided dialogues, to remove useless spaces and newlines).
	sf::Sprite _cursor;						//!< Dialogues cursor's sprite.
	sf::Sprite _answerCursor;				//!< Yes/No answers selection cursor's sprite.
	sf::Clock _cursorClock;					//!< Clock to handle dialogues's cursor animation.
	bool _drawCursor;						//!< True if dialogues's cursor will be displayed in the current frame, false else.
	bool _cursorOnYes;						//!< True if YES answer is select, false if NO answer is selected.
	bool _answerGiven;						//!< True if player selected an answer, false else.

	void _drawRectangle();					//!< Draw dialogues's rectangle and cursor; draw answer's rectangle and cursor if needed, too.

public:
	/*! \brief Constructor
	 *	\param[in] window Pointer to game's render window.
	 */
	DialogueManager(sf::RenderWindow *window);

	/*! \brief Initialize dialogue interface.
	 *
	 *	Call it in State::init() in every state that will print dialogues.
	 *	\param[in] filepath Dialogue binary file's path.
	 *	\sa State::init()
	 */
	void init(std::string filepath);

	/*! \brief Handle game's events.
	 *
	 *	If spacebar key is pressed:
	 *	 - print whole dialogue, if it is currently printing with delay;
	 *	 - go to the next dialogue, if the current one has been completely printed.
	 *	 - select an answer, if they're shown.
	 *
	 *	If up/down arrow key is pressed, select a different answer.
	 *	Call it in State::events().
	 *	\sa State::events()
	 */
	void events();

	/*!	\brief Update dialogues.
	 *
	 *	Call this function in State::update().
	 *	\sa State::update() 
	 */
	void update();

	/*! \brief Draw current dialogue.
	 *
	 * Call this function in State::draw()
	 * \sa State::draw()
	 */
	void draw();

	/*! \brief Clean up dialogue's resources.
	 *
	 *	Call this function in State::cleanup()
	 *	\sa State::cleanup()
	 */
	void cleanup();

	/*! \brief Check if player has selected an answer.
	 *
	 *	Use getAnswer() to know which answer the player has selected.
	 *	\return True if player has selected an answer, false else.
	 *	\sa getAnswer()
	 */
	inline bool answerGiven() const{ return this->_answerGiven; }

	/* Returns true if player gives answer YES, false if player gives answer NO.
	Use 'answerGiven()' to know if player has given an answer. */
	/*! \brief Get player's answer.
	 *
	 *	Use answerGiven() to know if player has selected an answer.
	 *	After you call this function, answerGiven() will return false until player select
	 *	another answer in on of the next dialogues.
	 *	However, you can call getAnswer() more than one times, if you're sure you're checking
	 *	the correct answer.
	 *	If no answer has ever been selected, this function return true.
	 *	\return True if player selected YES answer; false if he selected NO.
	 *	\sa answerGiven()
	 */
	inline bool getAnswer() { this->_answerGiven = false;  return this->_cursorOnYes; }
	
	/*! \brief Check if any dialogue is currently being printed.
	*	\return True if any dialogues is being printed, false else.
	*/
	inline bool working() const { return this->_working; }

	/*! \brief Start a dialogues chain, beginning from the one with given uid.
	 *
	 *	Next dialogues to show are retrieved using Dialogue::next_uid directive.
	 *	\param[in] uid UID of the first dialogue to show.
	 *	\sa Dialogue::next_uid start(Dialogue*)
	 */
	void start(int uid);

	/*! \brief Start a dialogues chain, beginning from the given one.
	*
	*	Next dialogues to show are retrieved using Dialogue::next_uid directive.
	*	\param[in] dialogue Pointer to the first dialogue to show.
	*	\sa Dialogue::next_uid start(int)
	*/
	void start(Dialogue *dialogue);

	/*! Show dialogue with given uid.
	 *
	 *	Dialogue::next_uid directive is ignored.
	 *	\sa alone(Dialogue*)
	 */
	void alone(int uid);

	/*! Show given dialogue.
	*
	*	Dialogue::next_uid directive is ignored.
	*	\sa alone(int)
	*/
	void alone(Dialogue *dialogue);

	/*! Get the UID of the last showed dialogue.
	 * \return UID of the last showed dialogue.
	 */
	inline int lastUID() const{ return this->_dialogue.uid; }

};

#endif