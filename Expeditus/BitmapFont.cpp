/*
* BitmapFont.cpp
* Project: Expeditus
* Author: Michele Marolla (mic.marolla@gmail.com)
*/

#include "BitmapFont.h"
#include <string>
#include <SFML/Graphics.hpp>
#include "Log.h"

BitmapFont::BitmapFont(){
	this->_ready = false;
	this->_index = 0;
	this->_target = 0;
	this->_delay = -1;
	this->_limitRect.left = this->_limitRect.top = -1;
	this->_limitRect.width = this->_limitRect.height = 0;
}

void BitmapFont::load(std::string texture_path, const sf::Color& color){
	Log::get() << "Loading image from '" << texture_path << "'...";
	if (!this->_font.loadFromFile(texture_path)){
		Log::get().newline();  Log::get().log("Unable to load texture", Log::flags::error); Log::get().newline();
		return;
	}
	Log::get() << "done."; Log::get().newline();
	this->_background = color;
	this->_w = this->_font.getSize().x / 16;
	this->_h = this->_font.getSize().y / 14;

	/* Build font */
	int top = this->_h;
	int baseA = this->_h;
	int currentChar = 0;
	
	for (int rows = 0; rows < 14; rows++){
		for (int cols = 0; cols < 16; cols++){
			// Set character offset and dimension
			this->_rects[currentChar].left = this->_w * cols;
			this->_rects[currentChar].top = this->_h * rows;
			this->_rects[currentChar].width = this->_w;
			this->_rects[currentChar].height = this->_h;

			/* Look for unique character clip */
			// Left side
			for (int pCol = 0; pCol < this->_w; pCol++){
				for (int pRow = 0; pRow < this->_h; pRow++){
					// Pixel offset
					int pX = this->_w * cols + pCol;
					int pY = this->_h * rows + pRow;
					// Character found!
					if (this->_font.getPixel(pX, pY) != this->_background){
						this->_rects[currentChar].left = pX;
						// Break loops
						pCol = this->_w;
						pRow = this->_h;
					}
				}
			}

			// Right side
			for (int pCol = this->_w - 1; pCol >= 0; pCol--){
				for (int pRow = 0; pRow < this->_h; pRow++){
					// Pixel offset
					int pX = this->_w * cols + pCol;
					int pY = this->_h * rows + pRow;
					// Character found!
					if (this->_font.getPixel(pX, pY) != this->_background){
						this->_rects[currentChar].width = pX - this->_rects[currentChar].left + 1;
						// Break loops
						pCol = -1;
						pRow = this->_h;
					}
				}
			}

			// Find top
			for (int pRow = 0; pRow < this->_h; pRow++){
				for (int pCol = 0; pCol < this->_w; pCol++){
					// Pixel offset
					int pX = this->_w * cols + pCol;
					int pY = this->_h * rows + pRow;
					// Character found!
					if (this->_font.getPixel(pX, pY) != this->_background){
						if (pRow < top)
							top = pRow;
						// Break loops
						pCol = this->_w;
						pRow = this->_h;
					}
				}
			}

			// Find bottom of A
			if (currentChar == 'A' - 32){ /* There are 14 rows, not 16*/
				for (int pRow = this->_h - 1; pRow >= 0; pRow--){
					for (int pCol = 0; pCol < this->_w; pCol++){
						int pX = this->_w * cols + pCol;
						int pY = this->_h * rows + pRow;
						// Character found!
						if (this->_font.getPixel(pX, pY) != this->_background){
							baseA = pRow;
							// Break loops
							pRow = -1;
							pCol = this->_w;
						}
					}
				}
			}
			currentChar++;
		}
	}

	// Calculate space and newline
	this->_space = this->_w / 2;
	this->_newline = baseA - top + 3;

	// Remove excess top pixels
	for (int i = 0; i < 224; i++){
		this->_rects[i].top += top;
		this->_rects[i].height -= top;
	}

	// Creating texture and sprite
	this->_texture.loadFromImage(this->_font);
	this->_sprite.setTexture(this->_texture);

	this->_ready = true;
}

sf::Vector2i BitmapFont::print(sf::RenderTarget *target, std::string text, int x, int y){
	if (!this->_ready){
		Log::get().log("Tried to use a bitmap font without any texture loaded", Log::flags::error);
		return sf::Vector2i(-1, -1);
	}

	/* Check limit */
	BitmapFont::limit_info problem = this->staysInLimit(text, x, y);
	if (problem.type == problem.x_exceeded){ /* Text exceeds x limit */
		/* Look for a space character */
		for (int i = problem.index; i >= 0, text[i] != '\n' ; i--){
			if (text[i] == ' '){ /* Change it with a new line */
				text[i] = '\n';
				return this->print(target, text, x, y);
			}
		}
		/* If space character was not found, truncate the text and go to new line*/
		text.insert(problem.index, 1, '\n');
		return this->print(target, text, x, y);

	}else if(problem.type == problem.y_exceeded){ /* Text exceeds y limit */
		/* Truncate text */
		text = text.substr(0, problem.index);
		return this->print(target, text, x, y);

	/* Everything is ok: print the text */
	}else{
		int tempX = x, tempY = y;
		for (unsigned int i = 0; i < text.length(); i++){
			if (text[i] == ' '){
				tempX += this->_space;
			}
			else if (text[i] == '\n'){
				tempY += this->_newline;
				tempX = (this->_limitRect.left != -1) ? this->_limitRect.left : x;
			}
			else{
				int ascii = (unsigned char)text[i] - 32; // 14 rows, not 16
				this->_sprite.setTextureRect(this->_rects[ascii]);
				this->_sprite.setPosition(tempX, tempY);
				target->draw(this->_sprite);
				tempX += this->_rects[ascii].width + 1; // 1px of padding
			}
		}
		return sf::Vector2i(tempX, tempY);
	}

	// It never reachs this
	return sf::Vector2i(-1, -1);
}

void BitmapFont::startDelay(sf::RenderTarget *target, std::string text, int delay){
	this->_target = target;
	this->_delay = delay;
	this->_index = 0;
	this->_text = text;
	this->_clock.restart();
}

sf::Vector2i BitmapFont::printWithDelay(int x, int y){
	if (this->_delay == -1)
		return sf::Vector2i(-1, -1);

	/* Print */
	if (this->_delay == -1 || this->_index >= this->_text.length())
		return this->print(this->_target, this->_text, x, y);
	unsigned int n = this->_clock.getElapsedTime().asMilliseconds() / this->_delay;
	this->_index = (n <= this->_text.length()) ? n : this->_text.length();
	return this->print(this->_target, this->_text.substr(0, this->_index), x, y);
}

void BitmapFont::stopDelay(){
	this->_delay = -1;
}

void BitmapFont::setLimit(int x1, int y1, int x2, int y2){
	this->_limitRect.left = x1;
	this->_limitRect.top = y1;
	this->_limitRect.width = x2 - x1;
	this->_limitRect.height = y2 - y1;
}

BitmapFont::limit_info BitmapFont::staysInLimit(std::string text, int x, int y){
	BitmapFont::limit_info limit;
	if (!this->_ready){
		Log::get().log("Tried to use a bitmap font without any texture loaded", Log::flags::error);
		return limit;
	}
	int tempX = x, tempY = y;
	int letterHeight;
	for (unsigned int i = 0; i < text.length(); i++){
		letterHeight = 0;
		if (text[i] == ' '){
			tempX += this->_space;
		}
		else if (text[i] == '\n'){
			tempY += this->_newline;
			tempX = this->_limitRect.left;
		}
		else{
			int ascii = (unsigned char)text[i] - 32; // 14 rows, not 16
			tempX += this->_rects[ascii].width + 1; // 1px of padding
			letterHeight = this->_rects[ascii].height;
		}
		
		if (tempX > (this->_limitRect.left + this->_limitRect.width)){
			limit.type = limit.x_exceeded;
			limit.index = i;
			return limit;
		}
		if (tempY + letterHeight > (this->_limitRect.top + this->_limitRect.height)){
			limit.type = limit.y_exceeded;
			limit.index = i;
			return limit;
		}
	}
	return limit;
}

int BitmapFont::exceedY(std::string text, int x, int y){
	/* Check limit */
	BitmapFont::limit_info problem = this->staysInLimit(text, x, y);
	while (problem.type == BitmapFont::limit_info::x_exceeded){ /* Text exceeds x limit */
		/* Look for a space character */
		int i;
		for (i = problem.index; i >= 0, text[i] != '\n'; i--){
			if (text[i] == ' '){ /* Change it with a new line */
				text[i] = '\n';
				break;
			}
		}
		/* If space character was not found, truncate the text and go to new line*/
		if (i == 0)
			text.insert(problem.index, 1, '\n');

		problem = this->staysInLimit(text, x, y);
	}

	return problem.index;
}

void BitmapFont::delLimit(){
	this->_limitRect.left = this->_limitRect.top = -1;
	this->_limitRect.width = this->_limitRect.height = 0;
}