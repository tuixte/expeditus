/*
 * Player.h
 * Project: Expeditus
 * Author: Michele Marolla (mic.marolla@gmail.com)
 */

#ifndef _PLAYER_H_
#define _PLAYER_H

#include <string>
#include "Character.h"
#include "Deck.h"
#include "SavingInterface.h"

/*! \brief Simple structure that store player's information. */
class Player : public Character{

	friend class SavingInterface;
protected:
	std::string name; //!< Player's name

private:
	album_t _album; //!< Player's card album

public:
	/*!	\brief Constructor
	 *	\param[in] uid Player's uid
	 */
	Player(int uid);

	/*! \brief Get player's name.
	 *	\return Player's name
	 *	\sa [setName]
	 */
	inline std::string getName() const{ return this->name; }

	/*! \brief Set player's name.
	 *	\param[in] name Player's name.
	 *	\sa getName()
	 */
	void setName(std::string name);

	/*! \brief Get player's album
	 *	\return Player's album. It's an array with the number of every card
	 *	of a certain uid owned by the player.
	 *	\sa addCard() removeCard() owned()
	 */
	inline album_t getAlbum(){ return this->_album; }

	/*! \brief Add a card to the player's album.
	 *	\param[in] id UID of the new card owned by the player.
	 *	\return True if the card has been successfully added, false else.
	 *	\sa getAlbum() removeCard() owned()
	 */
	bool addCard(int id);

	/*! \brief Remove a card from the player's album.
	 *	\param[in] id UID of the card you want to remove.
	 *	\return True if the card has been successfully removed, false else.
	 *	\sa getAlbum() addCard() owned()
	 */
	bool removeCard(int id);

	/*! \brief Get the number of card of a certain UID owned by the player.
	 *	\param[in] id UID of the card you want to get.
	 *	\return Number of the wanted card owned by the player.
	 *	\sa getAlbum() addCard() removeCard()
	 */
	int owned(int id);

};

#endif