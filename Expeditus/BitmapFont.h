/*
 * BitmapFont.h
 * Project: Expeditus
 * Author: Michele Marolla (mic.marolla@gmail.com)
 */

#ifndef _BITMAP_FONT_H_
#define _BITMAP_FONT_H_

#include <string>
#include <SFML\Graphics.hpp>
#include "SpriteSheet.h"

/*!	\brief Handles bitmap fonts.
 *
 * It only supports fonts with 14 rows and 16 columns, and characters arranged in ascii order.
 */
class BitmapFont{

private:
	bool _ready;				//!< True if any font was loaded, false else
	/* \brief Image that contains font.
	 *
	 * Used because BitmapFont works directly on pixels. */
	sf::Image _font;
	sf::Texture _texture;		//!< Font texture.
	sf::Sprite _sprite;			//!< Sprite that contains the single char to display.
	sf::Color _background;		//!< Font background color.
	int _w;						//!< Font image's width.
	int _h;						//!< Font image's height.
	int _newline;				//!< Distance beetween two lines.
	int _space;					//!< Distance beetween two spaced chars.
	sf::IntRect _limitRect;		//!< Rectangle in which text is limited to stay.
	int _index;					//!< Index of the last char displayed when printing text with delay \sa startDelay() printWithDelay()
	int _delay;					//!< Delay in milliseconds used when printing text with delay. \sa startDelay() printWithDelay()
	sf::IntRect _rects[224];	//!< Position and size of every character in font texture.
	sf::RenderTarget *_target;	//!< Pointer to target in which text will be rendered.
	std::string _text;			//!< Text to print with delay. \sa startDelay() printWithDelay()
	sf::Clock _clock;			//!< Clock used to handle delay printing. \sa StartDelay() printWithDelay()

public:
	//! Structure that stores information about text limit exceeding.
	struct limit_info{
		//! Type of limit-exceeding
		enum{
			none,		//!< Text does not exceed limit
			x_exceeded, //!< Text exceeds x-limit
			y_exceeded	//!< Text exceeds y-limit
		} type = none;
		int index = -1;	//! Index of the text's first char that exceeds limit
	};

public:
	//! Constructor
	BitmapFont();

	/*! \brief Load bitmap font from file
	 *	\param[in] texture_path Font file path
	 *	\param[in] color Font background color
	 */
	void load(std::string texture_path, const sf::Color& color);

	/*! \brief Check if font file has been loaded.
	 *	\return True if any bitmap font file was loaded, false else.
	 *	\sa load() */
	inline bool isReady() const{ return this->_ready; }

	/*! \brief Print text with loaded font.
	 *	\param[in] target Pointer to target in which text will be rendered.
	 *	\param[in] text Text to print.
	 *	\param[in] x X coordinate of text.
	 *	\param[in] y Y cordinate of text.
	 *	\return X and y coordinate just after printed text. Print in this position to
	 *	append something to the text.
	 */
	sf::Vector2i print(sf::RenderTarget *target, std::string text, int x, int y);

	/*! \brief Starting printing text with delay.
	 *
	 *	This function is used internally by DialogueManager.
	 *	To use this function manually: call it only once, than call printWithDelay() in every drawing cycle.
	 *	\param[in] target Pointer to target in which text will be rendered.
	 *	\param[in] text Text to print
	 *	\param[in] delay Delay between printing each character (in milliseconds).
	 *	\sa printWithDelay() delayStarted() delayCompleted() stopDelay()
	 */
	void startDelay(sf::RenderTarget *target, std::string text, int delay);

	/*! \brief Print saved text with delay.
	 *
	 *	Call this function in every drawing cycle. To set text and delay, see startDelay().
	 *	This function is used internally by DialogueManager.
	 *	\param[in] x X coordinate of text.
	 *	\param[in] y Y coordinate of text.
	 *	\return X and y coordinate just after printed text. Print in this position to
	 *	append something to the text.
	 */
	sf::Vector2i printWithDelay(int x, int y);

	/*! \brief Check if a string is currently being printed with delay.
	 *	\return True if a string is being printed with delay; false else.
	 *	\sa startDelay() printWithDelay() delayCompleted() stopDelay()
	 */
	inline bool delayStarted() const { return this->_delay != -1; }

	/* Return true if string was completely printed with delay; false else. */
	/*! \brief Check if text was completely printed.
	 *
	 *	Use this function only for text printed with delay.
	 *	\return True if a string has been completely printed with delay; false else.
	 *	\sa startDelay() printwithDelay() delayStarted() stopDelay()
	 */
	inline bool delayCompleted() const { return this->_index >= this->_text.length() -1; }

	/*! \brief Stop printing text with delay.
	 *	\sa startDelay() printWithDelay() delayStarted() delayCompleted()
	 */
	void stopDelay();

	/*! \brief Set rectangular limit in which text is forced to stay.
	 *
	 *	If text exceeds x limit, a new line character is inserted as soon as possibile: in particular,
	 *	the first space char is replaced by new line; if not found, text is truncated.
	 *  If text exceeds y limit, it is truncated and only visible part is printed.
	 *	\param[in] x1 X coordinate of limit rectangle's upper-left corner.
	 *	\param[in] y1 Y coordinate of limit rectangle's upper-left corner.
	 *	\param[in] x2 X coordinate of limit rectangle's bottom-right corner.
	 *	\param[in] y2 Y coordinate of limit rectangle's bottom-right corner.
	 *	\sa getLimit() staysInLimit() exceedY() delLimit()
	 */
	void setLimit(int x1, int y1, int x2, int y2);
	
	/*! \brief Get rectangular text limit.
	 *	\return Coordinates and size of rectangular text limit.
	 *	\sa setLimit() staysInLimit() exceedY() delLimit()
	 */
	inline sf::IntRect getLimit() const { return this->_limitRect; }

	/*!	\brief Check if a text will remain in limits, or not.
	 *	\param[in] text Text to check
	 *	\param[in] x Text's x coordinate
	 *	\param[in] y Text's y coordinate
	 *	\return limit_info default values (-1, -1, -1) if text stays in limit;
	 *	index of the character that exceeds limit, and which one (x or y) it exceeds.
	 *	\sa limit_info setLimit() getLimit() exceedY() delLimit()
	 */
	limit_info staysInLimit(std::string text, int x, int y);

	/* Returns -1 if 'text', printed beginning from (x, y) and adding newline character when it exceeds x-limit, stays in y-limit;
	otherwise, returns index of the character that exceed y-limit.
	If limit is not set, or font was not loaded, returns -1. */
	/*! \brief Check if text exceeds y-limit.
	 *
	 *	New-line characters will be added to text if it exceeds x-limit.
	 *	\param[in] text Text to check
	 *	\param[in] x Text's x coordinate
	 *	\param[in] y Text's y coordinate
	 *	\return Index of the first text's character that exceeds y-limit; -1 if text stays in limit.
	 *	\sa setLimit() getLimit() staysInLimit() delLimit()
	 */
	int exceedY(std::string text, int x, int y);

	/* Delete any limit to final text position. */
	/*! \brief Remove any limit.
	 *	\sa setLimit() getLimit() staysInLimit() exceedY()
	 */
	void delLimit();

};

#endif