/*
 * SpriteSheet.h
 * Project: Expeditus
 * Author: Michele Marolla (mic.marolla@gmail.com)
 */

#ifndef _SPRITE_SHEET_H_
#define _SPRITE_SHEET_H_

#include <string>
#include <SFML\Graphics.hpp>

/*! \brief Handle sprite-sheet files.
 *
 *	This class loads a texture from an image file, and gives you the opportunity to get only a part of it.
 *	Having a single texture and creating sprite getting only parts of it is a lot more efficient than to
 *	have a lot of different textures.
 */
class SpriteSheet{

private:
	/*! \brief True if any texture was loaded, false else.
	*
	*	It is used internally in get() function.
	*	\sa get()
	*/
	bool _ready;
	sf::Texture _texture;

public:
	/*! \brief Constructor */
	SpriteSheet();

	/*! \brief Load sprite sheet from path
	 *	\param[in] texture_path Texture filepath
	 */
	void load(std::string texture_path);

	/*! \brief Get part of texture.
	 *
	 *	Get a rectangular part of the loaded texture, and set it as sprite's texture.
	 *	If you have not loaded any texture, this function does nothing.
	 *	\param[out] sprite Sprite that will get the wanted texture part.
	 *	\param[in] x Starting x-coordinate of texture part
	 *	\param[in] y Starting y-coordinate of texture part
	 *	\param[in] w Width of texture part
	 *	\param[in] h Height of texture part
	 *	\sa load()
	 */
	void get(sf::Sprite *sprite, int x, int y, int w, int h);

};

#endif