/*
* Deck.cpp
* Project: Expeditus
* Author: Michele Marolla (mic.marolla@gmail.com)
*/

#include "Deck.h"
#include "Card.h"
#include "Consts.h"
#include "Log.h"

Deck::Deck(){
	this->_count = 0;
}

Card* Deck::getCardByUID(int uid){
	if (this->_count == 0 || uid <= 0)
		return 0;
	for (int i = 0; i < this->_count; i++){
		if (this->_deck[i].uid == uid)
			return &this->_deck[i];
	}
	return 0;
}

Card* Deck::getCardByPos(int pos){
	if (pos < 0 || pos >= this->_count)
		return 0;
	return &this->_deck[pos];
}

int Deck::addCard(Card &card){
	Log::get() << "Adding card uid " << card.uid << "into deck...";
	if (this->_count == EXPEDITUS_DECK_MAX_SIZE){
		Log::get().newline(); Log::get().log("Cannot insert card: deck full", Log::flags::error);
		return -1;
	}
	this->_deck[this->_count].uid = card.uid;
	this->_deck[this->_count].limit = card.limit;
	this->_deck[this->_count].name = card.name;
	this->_deck[this->_count].type = card.type;
	this->_deck[this->_count].desc = card.desc;
	this->_count++;

	Log::get() << "done."; Log::get().newline();

	return this->_count-1;
}

void Deck::delCardByUID(int uid){
	Log::get() << "Removing card uid " << uid << " from deck...";
	if (uid <= 0){
		Log::get().newline();  Log::get().log("Invalid uid", Log::flags::error); Log::get().newline();
		return;
	}
	for (int i = 0; i < this->_count; i++){
		if (this->_deck[i].uid == uid){
			if (this->_count == 1 || i == this->_count - 1) // If it's the only or the last card
				this->_deck[i].clear();
			else{
				this->_deck[i] = this->_deck[this->_count - 1]; // Put last card here
				this->_deck[this->_count - 1].clear();
			}
			this->_count--;

			Log::get() << "done."; Log::get().newline();

			return;
		}
	}

	Log::get().log("Card not found.", Log::flags::alert);
}

void Deck::delCardByPos(int pos){
	Log::get() << "Removing card in position " << pos << " from deck...";
	if (pos < 0 || pos >= this->_count){
		Log::get().newline();  Log::get().log("Invalid position", Log::flags::error); Log::get().newline();
		return;
	}
	Log::get() << " (uid " << this->_deck[pos].uid << ")...";
	if (this->_count == 1 || pos == this->_count - 1){ // If it's the only or the last card
		this->_deck[pos].clear();
	}else{
		this->_deck[pos] = this->_deck[this->_count - 1]; // Put last card here
		this->_deck[this->_count - 1].clear();
	}
	this->_count--;
	Log::get() << "done."; Log::get().newline();
}