/*
 * Character.h
 * Project: Expeditus
 * Author: Michele Marolla (mic.marolla@gmail.com)
 */

#ifndef _CHARACTER_H_
#define _CHARACTER_H_

#include <SFML\Graphics.hpp>
#include "Consts.h"
#include "SpriteSheet.h"

//! \brief Basic game character.
class Character : public sf::Drawable{

	friend class TiledMap;
protected:
	TiledMap *map;		//! Pointer to game's tiled map.

public:
	const int uid;		//! Character's uid.

private:
	sf::Texture _texture;	//!< Character's texture.
	sf::Sprite _sprite;		//!< Character's sprite.
	float _velocity;		//!< Character's velocity in tiles per second.
	float _offX, _offY;

public:
	/*! \brief Constructor
	 *	\param[in]uid Character's uid
	*/
	Character(int uid);

	/*! \brief Load texture from file.
	*	\param[in] filepath Texture file's path
	*/
	void load(std::string filepath);

	/*! \brief Get sprite from a sprite sheet.
	 *	\param[in] sheet SpriteSheet from which sprite will be retrieved
	 *	\param[in] x Sprite's x coordinate in sheet
	 *	\param[in] y Sprite's y coordinate in sheet
	 *	\param[in] w Sprite's width
	 *	\param[in] h Sprite's height
	 *	\sa SpriteSheet SpriteSheet::get()
	 */
	void getFromSheet(SpriteSheet *sheet, int x, int y, int w, int h);

	/*! \brief Get character's position.
	 *
	 *	Position is relative to game's world, not window.
	 *	\return Character's x and y coordinate.
	 *	\sa setPosition() move()
	 */
	sf::Vector2f getPosition() const;

	/*!	\brief Get sprite's size.
	 *	\return Sprite's width and height.
	 */
	sf::Vector2u getSize() const;
	
	/*! \brief Set character's position.
	 *
	 *	Position is relative to game's world, not window.
	 *	\param[in] x New character's x coordinate
	 *	\param[in] y New character's y coordinate
	 *	\sa setPosition(sf::Vector2i) getPosition() move()
	 */
	void setPosition(float x, float y);

	/*!\brief Set character's position.
	 *
	 *	Position is relative to game's world, not window.
	 *	\param[in] pos Character's x and y coordinates.
	 *	\sa setPosition(int, int) getPosition() move()
	 */
	void setPosition(sf::Vector2f pos);

	/*! \brief Set character's velocity (in pixels per second).
	*	\param[in] velocity Character's velocity in pixels per second.
	*	\sa setTileVelocity() getPixelVelocity() getTileVelocity
	*/
	void setPixelVelocity(float velocity);

	/*! \brief Set character's velocity (tiles per second).
	*	\param[in] velocity Character's velocity in tiles per second.
	*	\sa setPixelVelocity() getPixelVelocity() getTileVelocity()
	*/
	void setTileVelocity(float velocity);

	/*!	\brief Get character's velocity (in pixels per second).
	*	\return Character's velocity in pixels per second.
	*	\sa setPixelVelocity() setTileVelocity() getTileVelocity()
	*/
	inline float getPixelVelocity() const{ return this->_velocity; }

	/*!	\brief Get character's velocity (in tiles per second).
	*	\return Character's velocity in tiles per second.
	*	\sa setPixelVelocity() setTileVelocity() getPixelVelocity()
	*/
	inline float getTileVelocity() const{ return this->_velocity / EXPEDITUS_MAP_TILE_SIZE; }

	/*! \brief Move character (in pixels).
	 *
	 *	Position is relative to game's world, not window.
	 *	Given offsets are added to current character's coordinates.
	 *	\param[in] offX X offset
	 *	\param[in] offY Y offset
	 *	\sa getPosition() setPosition()
	 */
	void move(float offX, float offY);

	/*! \brief Move character (in tiles).
	*
	*	Position is relative to game's world, not window.
	*	Given offsets are added to current character's coordinates.
	*	\param[in] offX X offset
	*	\param[in] offY Y offset
	*	\sa getPosition() setPosition()
	*/
	void moveTiles(float offX, float offY);

	/*! \brief Check if the character's sprite touches the edges of another one.
	 *	\param[in] other Pointer to the other character you want to check
	 *	\return True if the two sprites touch each other, false else.
	 *	\sa collides()
	 */
	bool isNear(Character *other);

	/*!	\brief Check if this character's sprite collides with another one.
	 *	\param[in] other Pointer to the other character you want to check
	 *	\return True if the two sprites collide, false else.
	 *	\sa isNear()
	 */
	bool collides(Character *other);

	/*! \brief Get character sprite's global bounds.
	 *	\return Character sprite's global bounds.
	 */
	inline sf::FloatRect getBounds() const{ return this->_sprite.getGlobalBounds(); }

	/*! \brief Update character's position.
	 *	\param[in] s Seconds passed since the last drawing cycle. This argument is automatically given by TiledMap if you
	 *	use TiledMap::update() function (recommended).
	 *	\sa TiledMap::update()
	 */
	void update(float s);

	/*! \brief Draw character's sprite
	 *	\param[in] target Render target
	 *	\param[in] states Render states
	 *	\sa sf::Drawable::draw()
	 */
	virtual void draw(sf::RenderTarget &target, sf::RenderStates states) const;
};

#endif