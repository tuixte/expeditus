/* 
 * States.h
 * Project: Expeditus
 * Author: Michele Marolla (mic.marolla@gmail.com)
 */

/*! \file States.h
 *	\brief This header includes all the written states.
 */

#ifndef _STATES_H_
#define _STATES_H_

#include "IntroductionState.h"
#include "GameState.h"

#endif